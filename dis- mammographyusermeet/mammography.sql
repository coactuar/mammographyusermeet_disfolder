-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 20, 2022 at 03:06 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mammography`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pollanswers`
--

CREATE TABLE `tbl_pollanswers` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `poll_answer` varchar(10) NOT NULL,
  `poll_at` datetime NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_polls`
--

CREATE TABLE `tbl_polls` (
  `id` int(11) NOT NULL,
  `poll_question` varchar(500) NOT NULL,
  `poll_opt1` varchar(500) NOT NULL,
  `poll_opt2` varchar(500) NOT NULL,
  `poll_opt3` varchar(500) NOT NULL,
  `poll_opt4` varchar(500) NOT NULL,
  `correct_ans` varchar(10) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `poll_over` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_empid` varchar(200) NOT NULL,
  `user_question` varchar(200) NOT NULL,
  `asked_at` varchar(200) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reactions`
--

CREATE TABLE `tbl_reactions` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `reaction` varchar(50) NOT NULL,
  `reaction_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `ms` int(11) NOT NULL,
  `miss` int(11) NOT NULL,
  `mr` int(11) NOT NULL,
  `dr` int(11) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `mobile` varchar(200) NOT NULL,
  `organisation` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `login_date` varchar(200) DEFAULT NULL,
  `logout_date` varchar(200) DEFAULT NULL,
  `joining_date` varchar(200) NOT NULL,
  `logout_status` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `is_varified` int(11) NOT NULL DEFAULT '0',
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `ms`, `miss`, `mr`, `dr`, `fname`, `lname`, `mobile`, `organisation`, `city`, `login_date`, `logout_date`, `joining_date`, `logout_status`, `email`, `is_varified`, `password`) VALUES
(1, 0, 0, 1, 0, 'Akshat', 'Jharia', '07204420017', 'COACT', 'Bangalore', '2021/02/12 12:44:52', '2021/02/12 16:29:34', '2021/01/12 09:13:49', 0, 'akshat@coact.co.in', 0, '2ffddaed84e9e1c179a26fcc81ab7aff'),
(2, 0, 0, 1, 0, 'Akshat', 'Jharia', '07204420017', 'COACT', 'Bangalore', '2021/02/05 15:52:09', '2021/02/05 15:57:09', '2021/01/12 09:15:15', 0, 'akshatjharia@gmail.com', 0, 'c29e213bfd3209b2abdd23b53c62e0b0'),
(3, 0, 0, 1, 0, 'Nishanth', 's', '8747973536', 'coact', 'Bangalore', '2021/02/12 12:19:43', '2021/02/12 17:46:49', '2021/01/12 09:45:58', 0, 'nishanth@coact.co.in', 0, 'c8b4e9552c56ebc5b58538d29a8f37b8'),
(4, 0, 0, 1, 0, 'Sanket', 'Kathe ', '9769539333', 'Siemens Healthineers', 'Navi Mumbai', '2021/02/09 21:44:06', '2021/02/09 21:45:06', '2021/01/12 16:02:19', 0, 'sanketrkathe@gmail.com', 0, 'f8a209e428add085de29da004bb29a89'),
(5, 0, 0, 1, 0, 'Nishanth', 'S', '08747973536', 'Coact', 'Bangalore', NULL, NULL, '2021/01/14 18:36:29', 0, 'nishu_8989@yahoo.com', 0, 'a5262e30254a9d47f1c66513f4813d46'),
(6, 0, 0, 1, 0, 'Abhinav', 'Kaushik', '98406214863', 'Healthcare', 'Chennai', NULL, NULL, '2021/01/15 09:50:44', 0, 'abhinav.k89@gmail.com', 0, 'eeb23c515c0e08a283cf6cb2c9d573e6'),
(7, 0, 0, 1, 0, 'B', 'S', '9820325481', 'Self', 'MUMBAI', '2021/02/12 12:52:07', '2021/02/12 17:46:05', '2021/01/21 06:53:23', 0, 's.buddhe@rediffmail.com', 0, '09b6582aa59a44f682864b9cccf11d04'),
(8, 0, 0, 0, 1, 'Kameswari', 'Padmanabhan', '0000', 'HCG', 'Bangalore', '2021/02/12 16:59:55', '2021/02/12 17:44:48', '2021/01/21 06:57:13', 0, 'kameswarip87@gmail.com', 0, '7aedb6abfc6e1e0ab822f31f7c3156d7'),
(9, 0, 0, 0, 1, 'Padmaja', 'Canumalla', '4243537741', 'Teleradiology Solutions', 'Sammamish', '2021/02/12 20:33:56', '2021/02/12 20:34:56', '2021/01/21 07:52:03', 0, 'drcpadma@gmail.com', 0, '9ea1d8fae833433c2262530ebe56c275'),
(10, 0, 0, 0, 1, 'Suchana', 'Kushvaha', '9818374916', 'Nil', 'Delhi', NULL, NULL, '2021/01/21 10:02:27', 0, 'suchanakushvahadoc@gmail.com', 0, '4c8b6228df0ba568ce61b7d674fddf1f'),
(11, 0, 0, 0, 1, 'SUMA', 'CHAKRABARTHI', '9051076123', 'PEERLESS HOSPITAL', 'KOLKATA', '2021/02/12 14:54:56', '2021/02/12 15:33:39', '2021/01/21 10:56:09', 0, 'sumadoc@gmail.com', 0, 'b1a3929b1cd799a4e56b910537d52967'),
(12, 1, 0, 0, 0, 'Sariningsih', 'Hikmawati', '+628111282710', 'Dharmais Cancer center Hospital', 'Jakarta', NULL, NULL, '2021/01/21 13:18:41', 0, 'hsariningsih@yahoo.co.id', 0, '52db417d00f6659df5a5dd72b8b5f5a4'),
(13, 0, 0, 0, 1, 'Sudha', 'Karnan', '9789958860', 'Anderson Diagnostics ', 'Anderson ', NULL, NULL, '2021/01/21 14:14:34', 0, 'sudhadav08@gmail.com', 0, '4c48f4ef88bba7254ff8f16f570dd79b'),
(14, 0, 1, 0, 0, 'Pragati ', 'Kumari ', '9911443254', 'AIIMS,  New Delhi ', 'New Delhi ', NULL, NULL, '2021/01/21 18:18:25', 0, 'Pragatik432@gmail.com', 0, '7947d2ffeb6043f32e5bab6ee7ea0d28'),
(15, 0, 0, 0, 1, 'SEEMA', 'PANDEY', '09415224885', 'Kriti scanning', 'Allahabad', NULL, NULL, '2021/01/21 19:19:34', 0, 'docseemapandey@gmail.com', 0, '74119f229e3fe39fb96e2f69a5ad7590'),
(16, 0, 0, 0, 1, 'Saloni', 'Desai', '9767374700', 'Sir HN Reliance Foundation Hospital', 'Mumbai', NULL, NULL, '2021/01/21 20:15:28', 0, 'saludesai@gmail.com', 0, '33a375deec2345d224367edfbded5bea'),
(17, 0, 0, 0, 1, 'Ajaykumar', 'Thayilekandy', '09495151604', 'GOVT MEDICAL COLLEGE KOZHIKODE', 'kozhikode', NULL, NULL, '2021/01/21 20:22:51', 0, 'ajayrt@ymail.com', 0, 'fbc3d176c1f07ecc1611bf5ce75478b7'),
(18, 0, 0, 1, 0, 'Brijesh', 'Verma', '9971650038', 'SHPL', 'Varanasi', '2021/02/12 15:20:35', '2021/02/12 17:47:19', '2021/01/21 21:25:16', 0, 'brijesh.verma@siemens-healthineers.com', 0, '46e6b8263c114e7bc29aa91a7cff5533'),
(19, 0, 1, 0, 0, 'REKHA', 'KUNDU', '7797610775', 'SIEMENS', 'BENGALURU', NULL, NULL, '2021/01/22 07:12:22', 0, 'rekha.kundu1996@gmail.com', 0, '0233516a7d5a13484a63faa1c37ad7b7'),
(20, 1, 0, 0, 0, 'Mamta', 'Chand', '7498204611', 'NA', 'London', NULL, NULL, '2021/01/22 13:18:31', 0, 'mamta_lavina@yahoo.com', 0, 'a2e897787c54c8b38a6625e33dff1796'),
(21, 0, 0, 0, 1, 'Mathew', 'Cherian', '9600900373', 'KMCH ', 'Coimbatore', NULL, NULL, '2021/01/22 17:17:43', 0, 'dr.mathewcherian@gmail.com', 0, '31beb2ca1e924f41e45156b4edf322d6'),
(22, 0, 0, 1, 0, 'Karthik', 'KD', '7200363545', 'Smc', 'Chennai', '2021/02/12 16:28:32', '2021/02/12 17:30:53', '2021/01/22 17:31:59', 0, 'ktdkarthiktvr@gmail.com', 0, '02209817c06b8b4e35bbd6c9e49fc64f'),
(23, 0, 0, 1, 0, 'Akash ', 'Vegad', '9601951110', 'Lobo staffing', 'Rajkot', '2021/02/12 12:27:58', '2021/02/12 12:30:11', '2021/01/22 18:15:46', 0, 'vegadakash@gmail.com', 0, 'd6771720617c6b9ecfdbc7cdf8702b67'),
(24, 0, 0, 0, 1, 'Surina', 'Singhal', '9871463179', 'Max Hospital ', 'Gurgaon', NULL, NULL, '2021/01/22 19:00:13', 0, 'drsurinarad@gmail.com', 0, '3ef6b9c4cf03b082023a83d12011a060'),
(25, 0, 0, 1, 0, 'Dheeraj', 'Pandey', '9958025220', 'SCI,guwahti', 'Assam', '2021/02/12 13:36:53', '2021/02/12 13:37:53', '2021/01/23 09:11:36', 0, 'dheeraj.rpcaiims@gmail.com', 0, 'd26838941361bd09ca299cbb02e13214'),
(26, 0, 0, 0, 1, 'Elango', 'Nagappan', '9994277779', 'PSGIMSR', 'COIMBATORE', NULL, NULL, '2021/01/25 09:15:47', 0, 'cbethendral@gmail.com', 0, 'def6f20c8a5f2e611e140c770b2869cf'),
(27, 0, 0, 0, 1, 'Divya', 'Kantesariya', '9099988362', 'Rajkot Cancer Society', 'Rajkot', NULL, NULL, '2021/01/27 16:44:57', 0, 'dr.divya2212@gmail.com', 0, 'c5bfc0a88108d4f1a47b6fd67c0616ed'),
(28, 0, 0, 0, 1, 'Vijaykumar', 'Gupta', '9824311959', 'Rajkot Cancer Society', 'Rajkot', NULL, NULL, '2021/01/27 16:58:29', 0, 'drvkgupta2001@yahoo.com', 0, 'a1650ae04cfde5573997bec30c1c74f2'),
(29, 0, 1, 0, 0, 'Oshin', 'Mathew', '9620182681', 'Srinivas Institute of Medical science and research Center', 'Mangalore', NULL, NULL, '2021/01/27 20:58:09', 0, 'oshinmathew5@gmail.com', 0, '422a45e105ad360165012afd2fb5763a'),
(30, 0, 0, 0, 1, 'PREM', 'KUMAR', '09430212777', 'All India Institute of Medical Sciences, New Delhi', 'Patna', NULL, NULL, '2021/01/28 05:39:41', 0, 'drprempat@gmail.com', 0, '73bf3614f12b21d471b4b5f9bcfd99e4'),
(31, 0, 0, 0, 0, 'k', 'k', 'k', 'k', 'k', NULL, NULL, '2021/01/28 09:28:21', 0, 'k@k', 0, 'da0709044071cbb8ced21528f3e903ed'),
(32, 0, 0, 0, 1, 'Asif', 'Momin', '9867656698', 'Prince Aly khan hospital', 'Mumbai', NULL, NULL, '2021/01/28 10:55:47', 0, 'asifamomin@gmail.com', 0, 'aea81716870c28d398af417b74228eb7'),
(33, 0, 0, 0, 1, 'Janaki', 'P Dharmarajan', '9633009746', 'Amrita Institute of Medical Sciences', 'Kochi', NULL, NULL, '2021/01/28 12:13:00', 0, 'drjanakipd@gmail.com', 0, '039d7d5929c4a72c5fa1fbd662b45f75'),
(35, 0, 0, 1, 0, 'neeraj', 'reddy', '9700467764', 'coact', 'nellore', '2021/02/06 10:19:55', '2021/02/06 10:20:35', '2021/01/28 14:34:47', 0, 'neeraj@coact.co.in', 0, '2180d2beec45a1c6c94a7d96d77e9e4f'),
(36, 0, 0, 0, 1, 'Tulsio', 'Gwalani', '9822558090', 'Shivkala Diagnostic center ', 'Mumbai ', NULL, NULL, '2021/01/28 19:48:39', 0, 'drgwalani@gmail.com', 0, '75172628094dd4ca5683ba0715c9aed3'),
(37, 0, 0, 0, 1, 'Shankar', 'Kedia', '09331028411', 'Theism Ultra Sound Center', 'Kolkata 52', NULL, NULL, '2021/01/29 04:43:34', 0, 'kediafamily@ymail.com', 0, '9da680ea772702f32c1d74879ec44019'),
(38, 0, 1, 0, 0, 'Nimisha', 'Jais', '6291539451', 'state cancer institute', 'guwahati', NULL, NULL, '2021/01/30 09:56:32', 0, 'nimishajais@gmail.com', 0, 'd7f8b56a1ce36b76dc601bb2dac15fed'),
(39, 0, 0, 0, 1, 'Palak', 'Popat', '9930490036', 'Tata memorial Hospital', 'Mumbai', NULL, NULL, '2021/02/01 07:38:00', 0, 'dr.palakp@gmail.com', 0, 'e2bfd7d92f4816050b9565068c5a3c52'),
(40, 0, 0, 0, 1, 'Sudhakar ', 'Sampangi', '900880139', 'HCG KR unit', 'Bangalore', '2021/02/12 16:06:22', '2021/02/12 16:58:13', '2021/02/01 10:40:46', 0, 'dr_sudhakar79@yahoo.com', 0, '03051005e7373769ff56e39a49bc9244'),
(41, 1, 0, 0, 0, 'Rosebud', 'Gomes', '9821196504', 'Siemens Healthineers', 'Mumbai', '2021/02/12 12:43:52', '2021/02/12 17:47:35', '2021/02/01 11:36:38', 0, 'rosebud.gomes@siemens-healthineers.com', 0, '34f79194d03cec72dc1e99a04ee0767d'),
(42, 0, 1, 0, 0, 'Rakhi', 'Chadaga ', '99861 96913 ', 'Shpl', 'Bangalore ', NULL, NULL, '2021/02/01 12:51:34', 0, 'rakhishri3@gmail.com', 0, 'd8c782e6e0c6533aa2cdc83577aca999'),
(43, 1, 0, 0, 0, 'Poornima', 'Srinivasan', '9444048679', 'Frost & Sullivan', 'Chennai', '2021/02/12 12:49:39', '2021/02/12 14:58:23', '2021/02/01 14:07:35', 0, 'poornimas@frost.com', 0, '816c1428acba20d8a9a3de0558c7cbb6'),
(44, 0, 0, 1, 0, 'Swaminathan', 'N', '9003159027', 'Siemens Healthineers', 'Chennai', NULL, NULL, '2021/02/01 14:18:23', 0, 'njrupesh@gmail.com', 0, 'daac932abd64864a13ff545c37cdf24a'),
(45, 1, 0, 0, 0, 'Mary', 'Bhuyan', '09435030487', 'Radiodiagnosis', 'Dibrugarh', NULL, NULL, '2021/02/01 16:08:43', 0, 'marybhuyan123@gmail.com', 0, '0a16b8066337e373c138552db27906ca'),
(46, 1, 0, 0, 0, 'dr.v.santhi', 'veeraswamy', '09297101597', 'RITHIKA MATERNITY CLINIC ', 'Chittoor', NULL, NULL, '2021/02/01 16:31:27', 0, 'mallelarithika@gmail.com', 0, '3d1e4a1c0ca8f506682af1334106e2bd'),
(47, 0, 0, 0, 1, 'Hiral Jerajani', 'Kamath', '99445624', 'Aafiyaa Imaging & Diagnostic center', 'Muscat', '2021/02/12 14:17:57', '2021/02/12 14:33:07', '2021/02/01 17:41:09', 0, 'admin@aafiyaaimaging.com', 0, '8c53fa650549489c525e1ed7da8f1e3d'),
(48, 0, 0, 0, 1, 'Indu rani', 'Y', '9495903391', 'Cm hospital ', 'Pandalam, Pathanamthitta', NULL, NULL, '2021/02/01 18:45:00', 0, 'induranibyju@gmail.com', 0, 'fdcb27e8965e897fa91c8ad6835fa516'),
(49, 0, 0, 0, 1, 'Anitha', 'Sen', '9447455442', 'Rcc', 'Thiruvananthapuram', '2021/02/12 16:48:04', '2021/02/12 17:32:11', '2021/02/01 18:48:35', 0, 'dranithasen@hotmail.com', 0, '4b5e061c2c7cc9f15761ad2044d1a791'),
(50, 0, 0, 1, 0, 'Praveen', '.', '9716269583', 'Medanta the medicity', 'Haryana', NULL, NULL, '2021/02/01 19:02:42', 0, 'kohli.kohli42u@gmail.com', 0, '46865da9050b01bf9a5f15ebfbfba494'),
(51, 0, 0, 0, 1, 'Pramod', 'Singh', '9454915615', 'IMS bhu', 'Varanasi', NULL, NULL, '2021/02/01 21:06:51', 0, 'pksimsbhu@gmail.com', 0, 'ec73a04d7ae0eff3e66386b561550809'),
(52, 0, 0, 0, 1, 'darshan', 'thummar', '09725877042', 'GCRI', 'Ahmedabad', '2021/02/12 11:54:24', '2021/02/12 11:55:24', '2021/02/02 12:18:47', 0, 'darshan.thummar000@gmail.com', 0, '1ca03a3f0256ab2fe470f379590d3e3c'),
(53, 0, 0, 1, 0, 'Joydeep', 'Bhattacharjee', '9831152034', 'Siemens Healthcare ', 'Kolkata', NULL, NULL, '2021/02/02 13:20:47', 0, 'joydeep.bhattacharjee@siemens-healthineers.com', 0, 'e80ffdc42b6d5c6286e7bb2e932f21c0'),
(54, 0, 0, 1, 0, 'Satya', 'Mohapatra', '9439202009', 'IMS AND SUM HOSPITAL', 'Bhubaneswar', NULL, NULL, '2021/02/02 13:51:12', 0, 'satyamohapatra@soa.ac.in', 0, 'b0e7e9857730f7f62ff95e5f12e27aec'),
(55, 0, 0, 0, 1, 'Hari', 'Kishore', '9738483141', 'IMS SUM HOSPITAL', 'Bhubaneswar', NULL, NULL, '2021/02/02 13:51:33', 0, 'harikishore.e@gmail.com', 0, 'ed1b5571bb5d5bf3c67adedc30440a82'),
(56, 0, 1, 0, 0, 'Alayna', 'Reddy', '9618063641', 'SUM Hospital ', 'Bhubaneswar ', NULL, NULL, '2021/02/02 14:01:38', 0, 'shalu030@gmail.com', 0, 'ebb2cc55d094dc663b53ab21a7982f15'),
(57, 0, 0, 0, 1, 'kartikeya', 'kirti', '08375954367', 'safdarjung hospital', 'gurgaon', NULL, NULL, '2021/02/02 14:21:08', 0, 'kartik043@gmail.com', 0, 'f69eed2824425ca35c51027ce69d6775'),
(58, 0, 0, 0, 1, 'Geethanjali ', 'J', '9843394708', 'Sri Ramakrishna Hospital', 'Coimbatore', '2021/02/12 15:20:47', '2021/02/12 15:31:57', '2021/02/02 14:30:19', 0, 'geethanjalij@yahoo.co.in', 0, '360ef607cedb49a7ecfd2d5866fa8857'),
(59, 0, 0, 0, 1, 'Vinayaga Priya', 'Manoharan', '8220232999', 'Vaman Scans and Diagnostics', 'Coimbatore', NULL, NULL, '2021/02/02 14:34:11', 0, 'vinayagapriya@gmail.com', 0, '672d1cce7a683c907a55c64a61984f42'),
(60, 0, 0, 0, 1, 'Bhuvaneswari', 'S', '9487563235', 'Sudarsana Scans', 'Salem', NULL, NULL, '2021/02/02 14:43:38', 0, 'salemsudarsanascans@gmail.com', 0, '608a1ede384804cd202dcaf6fb9f2c77'),
(61, 0, 0, 0, 1, 'Vidhyapriya', 'R', '9894069690', 'PSG College of Technology', 'Coimbatore', NULL, NULL, '2021/02/02 14:49:42', 0, 'rvp.bme@psgtech.ac.in', 0, '99e1449b4febd05ab07be95bc3c7879f'),
(62, 0, 0, 0, 1, 'Devanand', 'B', '9842290345', 'PSG Hospitals', 'Coimbatore', NULL, NULL, '2021/02/02 14:53:07', 0, 'devanandb@hotmail.com', 0, '8b05b524b6fc3eb8311ff254d75f060b'),
(63, 0, 0, 0, 1, 'Suresh Kumaran', 'V', '0000000000', 'SKS Hospital', 'Salem', NULL, NULL, '2021/02/02 14:54:38', 0, 'skv@skshospital.org', 0, '4d46959416adf01461bc10a2d340c2de'),
(64, 0, 0, 0, 1, 'Vijay', 'S', '0000000000', 'SKS Hospital', 'Salem', NULL, NULL, '2021/02/02 14:55:27', 0, 'vijay@skshospital.org', 0, 'd4fded015f7fec83d874b642c8b681fb'),
(65, 1, 0, 0, 0, 'Aishwarya ', 'Singh', '7827522489', 'Ims and Sum Hospital ', 'Bhubaneswar ', NULL, NULL, '2021/02/02 15:02:07', 0, 'aish.dmims@gmail.com', 0, '9fb5dac19baf3353b772cd9c04daf736'),
(66, 0, 0, 0, 0, 'Niranjan', 'Sahu', '9861349333', 'IMS and SUM hospital', 'Bhubaneswar', NULL, NULL, '2021/02/02 15:05:26', 0, 'niranjanradiologist@gmail.com', 0, '49c9a8e875ad2e74382c953effb957ed'),
(67, 0, 0, 0, 1, 'PRATIK', 'KAPSE', '9764508607', 'DMIMS', 'Wardha', NULL, NULL, '2021/02/02 15:13:46', 0, 'pratikkapse58@gmail.com', 0, '5c00db86276a38547c264c3e222b066b'),
(68, 0, 0, 0, 1, 'Aarthi', 'Govind', '9940022447', 'Aarthi scans', 'Chennai', NULL, NULL, '2021/02/02 16:03:06', 0, 'aarthigovind@gmail.com', 0, '61ad1ab4a324552a546f2f85648d5804'),
(69, 0, 0, 0, 1, 'Anamika', 'Shahi', '08260175375', 'IMS & SUM Hospital', 'Bhubaneswar', NULL, NULL, '2021/02/02 16:07:31', 0, 'dranamikashahi92@gmail.com', 0, '6ad5e8cc18a6e196a685001b7d630eb9'),
(70, 0, 0, 0, 1, 'Abhijeet ', 'Joshi', '07750044887', 'Vikash hospital', 'Bargarh', NULL, NULL, '2021/02/02 19:12:32', 0, 'abhijeetjoshi1987@gmail.com', 0, '761d8ea186c1f8464a59b6b6270a6f44'),
(71, 0, 0, 1, 0, 'Vishnu', 'Priyan I', '7539964260', 'Radiology Technologist-Internship ', 'Chennai ', NULL, NULL, '2021/02/02 19:40:10', 0, 'vishnuvictores99@gmail.com', 0, '518af43dc7864cc83bc1bfbed611ea6e'),
(72, 0, 0, 0, 1, 'Jayaraj', 'Govindaraj', '9841014128', 'Apollo Cancer Centre', 'Chennai', NULL, NULL, '2021/02/02 20:57:37', 0, 'jayarajg@gmail.com', 0, 'ff6ec69052d6fde437bd68c28edc8b18'),
(73, 0, 0, 0, 1, 'Brig', 'Mohan', '09999984484', 'SGT Medical College, Hospital & Research Institute', 'Gurgaon', NULL, NULL, '2021/02/03 05:00:08', 0, 'brigcmohan@gmail.com', 0, '8c269e803e0610383a68f7596f4d33a3'),
(74, 0, 0, 0, 1, 'Yashpalsinh', 'Chauhan', '9624822684', 'Gujarat cancer and research institute', 'Ahmedabad', NULL, NULL, '2021/02/03 08:04:13', 0, 'yashpalchauhan266@gmail.com', 0, '6ba9bea09f113b7ec06f8a26099aee3f'),
(75, 1, 0, 0, 0, 'Sunetra', 'Mukherjee', '9830477112', 'Quadra Medical Services', 'Kolkata', NULL, NULL, '2021/02/03 09:21:00', 0, 'docsunetra@gmail.com', 0, '6544f03174144519b653c676af82607f'),
(76, 0, 0, 0, 1, 'Dr.sudhir', 'labana', '09537898706', 'Pratibha hospital', 'Dahod', NULL, NULL, '2021/02/03 11:00:51', 0, 'SUDHIRLABANA99@GMAIL.COM', 0, 'b1e068f6fd26b02ef58522a32939b135'),
(77, 0, 0, 0, 1, 'Pallavi', 'Dharmadhikari', '09923151111', 'Shree Saibaba Heart Institure', 'Nashik', NULL, NULL, '2021/02/03 11:05:01', 0, 'pallaviad@yahoo.com', 0, '9d08e343e73305e57601cea5fd294b6f'),
(78, 0, 0, 1, 0, 'jitendra', 'singh', '9835340033', 'chandrama imaging centre ', 'RANCHI', NULL, NULL, '2021/02/03 11:43:51', 0, 'endmasingh@gmail.com', 0, 'efb87ee6e533fd8b082edb675716ed25'),
(79, 0, 0, 0, 1, 'AMEE', 'PANCHAL', '9979039298', 'Zydus cancer centre', 'Ahmedabad', NULL, NULL, '2021/02/03 12:19:20', 0, 'doctoramee@gmail.com', 0, '641281c71e3c3e511c38773316640cc1'),
(80, 0, 0, 0, 1, 'DIJENDRA NATH', 'BISWAS', '07501248601', 'IPGMER and SSKM hospital', 'Kolkata', NULL, NULL, '2021/02/03 14:08:07', 0, 'dijencnmc@gmail.com', 0, '00c257c4025718ad2ec1142979546ac6'),
(81, 0, 0, 1, 0, 'Tusar Kumar', 'Das', '8293197925 ', 'Pgt', 'Kolkata', NULL, NULL, '2021/02/03 14:20:17', 0, 'tusard2@gmail.com', 0, 'd556f1a10b1a9a31c958b170c14c51bf'),
(82, 0, 0, 1, 0, 'Pran Krishna', 'Majumdar', '9830034721', 'KPC MEDICAL COLLEGE AND HOSPITAL', 'Kolkata', NULL, NULL, '2021/02/03 14:34:18', 0, 'pranmajumdar59@gmail.com', 0, 'd7703a585ea6b47e5efcc1a64cda360f'),
(83, 1, 0, 0, 0, 'Nupur', 'Patel', '9898413838', 'Mammography', 'Ahmedabad', NULL, NULL, '2021/02/03 20:40:35', 0, 'nupur.radio@gmail.com', 0, '54a5d08813084a87a1d4f905e48e8f0d'),
(84, 0, 0, 1, 0, 'Ashok Kumar', 'Gupta', '9839333013', 'Siemens healthcare ', 'Lucknow ', NULL, NULL, '2021/02/04 07:32:42', 0, 'ashokkumar.gupta@siemens-healthineers.com', 0, '1524edb0c2bf08d35fc69950b494f919'),
(85, 0, 0, 0, 1, 'PinakPani ', 'Bhattacharya', '9830166024', 'Quadra Medical services ', 'Kolkata ', NULL, NULL, '2021/02/04 09:04:32', 0, 'pinakpanidr@hotmail.com', 0, '011180a683fbac1508657ec017e6f66a'),
(86, 0, 0, 1, 0, 'Subhasish', 'Ghosh', '9830909440', 'Siemens Healthcare ', 'Kolkata ', NULL, NULL, '2021/02/04 09:21:08', 0, 'subhasish.ghosh@siemens.com', 0, 'd9ab34d13149375e4adad41c14dedde4'),
(87, 0, 0, 0, 1, 'Archana ', 'Rao', '9482229935', 'MMCRI', 'Mysore', NULL, NULL, '2021/02/04 09:24:19', 0, 'archanarao311@gmail.com', 0, '7ebbe7c912f6fc66c2c833c7a9c0dbe4'),
(88, 0, 0, 0, 1, 'Padmini ', 'S G', '9482754954', 'MMCRI', 'Mysore', NULL, NULL, '2021/02/04 09:25:27', 0, 'gopupa2012@gmail.com', 0, '3bdfde613d1feb36686c3fa938fe2b97'),
(89, 0, 0, 0, 1, 'Tanuja', 'C p', '9008410291', 'Mmcri', 'Mysore', NULL, NULL, '2021/02/04 09:25:35', 0, 'iamkodavathi@gmail.com', 0, 'adff70358e087f8a7a9bd2cc3de6516e'),
(90, 0, 0, 1, 0, 'Nikesh', 'Varughese', '9902766227', 'Siemens Healthineers', 'Bangalore', NULL, NULL, '2021/02/04 09:41:19', 0, 'nikesh.varughese@siemens-healthineers.com', 0, 'c6f8b537c7879660e01949ab11cdf874'),
(91, 1, 0, 0, 0, 'Mizaj', 'Ismail', '096879216907', 'Iria', 'Oman', NULL, NULL, '2021/02/04 12:56:34', 0, 'mizajismails@gmail.com', 0, '92ac189621743ba484c042409a10907d'),
(92, 0, 0, 1, 0, 'Mahendran', 'Gopalakrishnan', '9940307618', 'Siemens', 'Chennai', NULL, NULL, '2021/02/04 13:12:15', 0, 'gopalakrishnan@siemens-healthineers.com', 0, '8c895a1cd9ec0248c629bf846b1184c9'),
(93, 1, 0, 0, 0, 'RUPAL', 'JOSHI', '9727718657', 'AASTHA CLINIC', 'VADODARA', NULL, NULL, '2021/02/04 13:19:59', 0, 'docrupal@gmail.com', 0, '34f27010624480175ccbda377df3a084'),
(94, 1, 0, 0, 0, 'Jayshree', 'Patil', '9820877457', 'Sonoscan', 'Navi Mumbai ', NULL, NULL, '2021/02/04 16:46:05', 0, 'jayshreeankur@gmail.com', 0, '2fcfcb36bcdf42ef78d5f3ed8736a402'),
(95, 0, 0, 0, 1, 'PORKODI', 'D', '09842182763', 'Kilpauk Medical College', 'Chennai', NULL, NULL, '2021/02/04 17:15:40', 0, 'goldendoctor@yahoo.com', 0, 'cf4a28aa58b9564e11c6260a4e74e205'),
(96, 0, 0, 0, 1, 'Jaslyn', 'Devakirubai', '7094098464', 'Madurai medical college', 'Madurai', NULL, NULL, '2021/02/04 17:44:09', 0, 'jdevakirubai@rediffmail.com', 0, 'e9c0a466d3805e85c4a799aad188a9a1'),
(97, 0, 0, 0, 1, 'Bobby', 'Devasia', '9447228802', 'New navajeevan  scan  center ', 'Chalakudy', NULL, NULL, '2021/02/04 19:06:35', 0, 'drbobbydevasia@yahoo.co.uk', 0, 'aa7cb8f6e29537b966976207f666ce3f'),
(98, 0, 0, 0, 0, 'Debasis ', 'padhi ', '9437063320', 'Nidan ', 'Berhampur', NULL, NULL, '2021/02/04 22:29:20', 0, 'nidanpadhi@gmail.com', 0, '749ddab3a1b5800391cfdf623b7f3009'),
(99, 0, 0, 0, 1, 'Jincy', 'Mathew ', '9495335163', 'Caritas', 'Kottayam ', NULL, NULL, '2021/02/05 08:33:28', 0, 'jincyjojo1@gmail.com', 0, '96c5e43843013203a5a63d34a93d23c1'),
(100, 0, 0, 0, 1, 'Rekha', 'Narayanan', '9526101026', 'Malabar institute of medical sciences', 'Kozhikode', '2021/02/12 14:42:30', '2021/02/12 15:13:16', '2021/02/05 09:51:58', 0, 'rekhanar@gmail.com', 0, '6a9254d3c0fd857ee9d7286b65aa4c45'),
(101, 0, 0, 0, 1, 'Amol', 'More', '08390417575', 'OJAS Hospital', 'PIMPRI CHINCHWAD', NULL, NULL, '2021/02/05 10:04:17', 0, 'apmore_82@rediffmail.com', 0, 'dd454fbbea274e21da2c8079399b280b'),
(102, 0, 0, 1, 0, 'Santosh', 'Menon', '09392474617', 'MySage Hospital', 'Bhopal', NULL, NULL, '2021/02/05 10:12:16', 0, 'toshmani@hotmail.com', 0, '03884eccfc5fdcbb293840b8d0641093'),
(103, 0, 0, 0, 1, 'S.K.', 'Sharma', '9830027189', 'EKO XRAY', 'Kolkata', '2021/02/12 12:15:33', '2021/02/12 12:16:33', '2021/02/05 10:35:55', 0, 'drsksharma@ekoxray.com', 0, '4bb8881102c731b2d88ccb9d2d436d4b'),
(104, 0, 0, 0, 1, 'Venkatesh', 'Kasi ', '9940944588', 'KMCH', 'Coimbatore', NULL, NULL, '2021/02/05 10:44:08', 0, 'radvenki79@gmail.com', 0, '5b0d3fa40985b3be7d84f97ea0f16bd6'),
(105, 0, 0, 0, 1, 'Shajeev', 'Jaikumar', '09500017295', 'Madras Medical Mission Hospital.', 'Chennai', NULL, NULL, '2021/02/05 11:19:39', 0, 'dr.shajeevv@gmail.com', 0, '628d56e99ae5c3c2bef2cec85f56fbb3'),
(106, 0, 0, 0, 0, 'Sangeeta', 'Saxena', '9829039999', 'Govt Medical College Kota India', 'Kota', '2021/02/12 17:40:29', '2021/02/12 17:41:29', '2021/02/05 11:26:32', 0, 'sangeetasaxena9@gmail.com', 0, '4b03393a3655978f7e4b4ae1640bc9be'),
(107, 0, 0, 0, 1, 'Yogendra', 'Sachdev', '09850746298', 'Rural medical college Loni ', 'Rahata ahmednagar maharashtra', NULL, NULL, '2021/02/05 12:50:31', 0, 'yogendrasachdev@gmail.com', 0, 'c90e388b9ac66f8e010934349a06aac1'),
(108, 0, 0, 0, 1, 'Naresh', 'Hundiya', '09825348408', 'DrNareshhundiya imaging centre', 'Nadiad', NULL, NULL, '2021/02/05 14:23:43', 0, 'drnareshhundiya@gmail.com', 0, '9a9dd4f6a20902c347e4fee795feb6d1'),
(109, 0, 0, 1, 0, 'Karanbir', 'Ahluwalia', '8283835389', 'Siemens', 'Gurgaon', '2021/02/12 15:54:06', '2021/02/12 16:58:49', '2021/02/05 15:34:46', 0, 'karanbir.ahluwalia@gmail.com', 0, '6d5c8ee5999be33afb578ef23a23f36b'),
(110, 0, 1, 0, 0, 'Pooja', 'Jaiswal', '09768161921', 'COACT', 'Mumbai', '2021/02/12 12:30:44', '2021/02/12 17:43:01', '2021/02/05 16:14:32', 0, 'pooja@coact.co.in', 0, '4a61b2f6248dba00390bfdb3114c4d74'),
(111, 0, 0, 1, 0, 'Deepak', 'Sharma', '9119732271', 'Bimr hospital', 'Gwaliour', NULL, NULL, '2021/02/05 17:08:06', 0, 'sharmadpk234@gmail.com', 0, '026ff84e01a56e310dbd149e4a2594a3'),
(112, 0, 0, 0, 1, 'V. K.', 'Agarwal', '9415214732', 'Kriti Scanning Pvt Ltd', 'Prayagraj,U.P.', NULL, NULL, '2021/02/05 18:05:16', 0, 'kritiscan@gmail.com', 0, 'b79ce2dc93dc8f6e34fd15709e361a5a'),
(113, 0, 1, 0, 0, 'Neepa', 'Shah', '9825136886', 'Womenâ€™s Health care clinic ', 'Surat ', '2021/02/12 14:51:50', '2021/02/12 15:24:17', '2021/02/05 18:11:51', 0, 'neepa30@gmail.com', 0, '8021221e51da409828fd0bce29869d43'),
(114, 0, 0, 0, 1, 'Kushagra', 'Agarwal', '8004373099', 'Kriti Scanning Pvt. Ltd.', 'Allahabad', NULL, NULL, '2021/02/05 18:16:11', 0, 'kushagra2938@gmail.com', 0, '0c677ce04cdb661dd8ce5b56cb657da5'),
(115, 0, 0, 0, 0, 'Jwala', 'Srikala ', '9948156249', 'Kims', 'Hyderabad', NULL, NULL, '2021/02/05 18:40:18', 0, 'srikalajwala@yahoo.com', 0, '3386109af70c97e91714f6db08adfa25'),
(116, 0, 0, 0, 1, 'Ashwini ', 'Umredkar ', '9822909771', 'AIIMS ', 'NAGPUR ', NULL, NULL, '2021/02/06 05:47:15', 0, 'ashwiniumredkar@yahoo.co.in', 0, '5e380d8cbb287cbfc72af50498142024'),
(117, 0, 0, 1, 0, 'Vikas ', 'Nayak ', '9820112578', 'Siemens ', 'Mumbai ', NULL, NULL, '2021/02/06 08:24:15', 0, 'vikas.nayak@siemens-healthineers.com', 0, '0f497a9883d69043e42a9dd0a81a2fd6'),
(118, 0, 0, 1, 0, 'Ashutosh ', 'Singh', '9096301571', 'Siemens Healthineers', 'Indore', NULL, NULL, '2021/02/06 08:49:40', 0, 'ashutoshsingh@siemens-healthineers.com', 0, 'abdf361dc8f05f30732b18858a59d553'),
(119, 0, 0, 1, 0, 'Viraj', 'Khot', '9765875731', 'COACT', 'Pune', '2021/02/06 16:42:01', '2021/02/06 17:07:42', '2021/02/06 16:36:45', 0, 'viraj@coact.co.in', 0, 'c925e6b668b60ad847e5abbca321179e'),
(120, 0, 0, 0, 1, 'Soumya ', 'Stephen ', '8547082313', 'Lifecare scan centre ', 'Thrissur ', NULL, NULL, '2021/02/06 20:38:52', 0, 'sstephen083@gmail.com', 0, 'afd6b7588b02dd1042667f13d1553f73'),
(121, 0, 0, 1, 0, 'Rakesh', 'Patwa', '8980571077', 'Lions hospital', 'MEHSANA', NULL, NULL, '2021/02/07 07:32:56', 0, 'patwarp@gmail.com', 0, 'a15da123bdce79538331fe3482c61df6'),
(122, 0, 0, 0, 1, 'Ajaykumar', 'Chaudhari', '08000035624', 'Sattva Imaging Centre', 'Gandhi Nagar', NULL, NULL, '2021/02/07 08:25:33', 0, 'acesajay3@gmail.com', 0, 'e8c4b96de2957d869c1b91815a14964c'),
(123, 0, 0, 1, 0, 'Kiran ', 'Doke', '7419122828', 'Siemens Healthineers', 'Udaipur', '2021/02/12 13:09:05', '2021/02/12 14:39:19', '2021/02/07 09:47:11', 0, 'kiran.doke@siemens-healthineers.com', 0, '3fa2625e1d68d5c594ddefa7ed7a2909'),
(124, 0, 0, 0, 1, 'Kushal', 'Gehlot', '9413107135', 'RNT Medical College', 'Udaipur', NULL, NULL, '2021/02/07 09:53:03', 0, 'drkushalgehlot@gmail.com', 0, '1f93e907337fd82b688a4db2c061de07'),
(125, 0, 0, 0, 1, 'Amritha', 'Jayakumar', '09495501231', 'Pulse doctors and scans', 'Palakkad', NULL, NULL, '2021/02/07 09:59:26', 0, 'amrithajay@yahoo.co.in', 0, 'd43045147c22342a8aa8938e5c8b918c'),
(126, 0, 0, 0, 1, 'Ravindar ', 'Kundu', '9571218953', 'Geetanjali Hospital', 'Udaipur', NULL, NULL, '2021/02/07 10:14:53', 0, 'kundu19@yahoo.in', 0, '9ed725b32c8f7c5485b8ca5f294c8ee3'),
(127, 0, 0, 0, 1, 'Hariram', 'Chaudhary', '7568040029', 'Geetanjali Hospital', 'Udaipur', NULL, NULL, '2021/02/07 10:16:36', 0, 'harry.bjmc@gmail.com', 0, '20acc9f80aa1d8bcfa68843864f13708'),
(128, 0, 0, 0, 1, 'Bharat', 'Jain', '9159486791', 'Kshipra Scans and Labs', 'Udaipur', NULL, NULL, '2021/02/07 10:17:50', 0, 'drbharatjain10@gmail.com', 0, '814487352b488a56c49f8100a10c0ede'),
(129, 0, 0, 0, 1, 'Archana', 'Bhadada', '7506588196', 'Apex Sonography', 'Udaipur', NULL, NULL, '2021/02/07 10:20:51', 0, 'jbhadada5@gmail.com', 0, 'd1c7293748ca5a230f8203f48765abc5'),
(130, 0, 0, 1, 0, 'raman', 'Kumar', '9815617653', 'Siemens Healthcare Private Limited ', 'Hoshiarpur', NULL, NULL, '2021/02/07 14:38:37', 0, 'raman.kumar@siemens-healthineers.com', 0, '935e11dc6c06b2e90d2f1f53c8f4e54e'),
(131, 0, 0, 0, 1, 'Ashish', 'Man', '9417171365', 'Beas Diag.', 'Punjab', '2021/02/12 16:00:07', '2021/02/12 16:01:07', '2021/02/07 14:39:43', 0, 'beasdiagnostics@gmail.com', 0, '135fffc174b145a47f19e3f58615e378'),
(132, 0, 0, 0, 1, 'Vandana', 'Setia', '09815598230', 'Mohan dai oswal cancer hospital ', 'Ludhiana ', NULL, NULL, '2021/02/07 14:55:27', 0, 'vsetia@hotmail.com', 0, 'bb3b0178f777d824d49ff9efe4da0aca'),
(133, 0, 0, 0, 1, 'SIDHARTH', 'SHARMA', '7757071117', 'AIIMS', 'Raipur', NULL, NULL, '2021/02/07 16:25:14', 0, 'drsidharth13@gmail.com', 0, '762635197b43f18555cc5c0ebc529c7b'),
(134, 0, 0, 0, 1, 'Namrata', 'Jagawat', '+91 95873 90716', 'Aayush Lab & Diagnostics ', 'Bikaner', NULL, NULL, '2021/02/07 16:50:28', 0, 'jagawat24@gmail.com', 0, 'b29e1a5f5ff6f1ab0ef8d6e6888c6409'),
(135, 0, 0, 0, 1, 'Simmi', 'Aggarwal', '09356912818', 'Garg  Hospital ', 'Faridkot', NULL, NULL, '2021/02/07 18:16:01', 0, 'drsimmigarg@gmail.com', 0, '303c78195263aeac0045b41255780567'),
(136, 0, 0, 1, 0, 'Rakesh ', 'Lakka', '9849807736', 'Glocal Digital Pvt Ltd ', 'Hyderabad ', NULL, NULL, '2021/02/08 12:16:56', 0, 'Rakeshlakka@glocaldigital.co.In', 0, '231213ccc382d377b7f4ce1b2aa3e475'),
(137, 0, 0, 0, 1, 'MURUGAN ', 'GOPALAKRISHNAN ', '9840107966', 'INDIAN SCAN ', 'CHENNAI ', NULL, NULL, '2021/02/08 14:05:16', 0, 'dr.gmurugan@yahoo.com', 0, '2b3c65b421b408d7f8ed0a934ef60e85'),
(138, 0, 0, 0, 1, 'Yashvantlal ', 'Patel', '9825045625', 'Manglam diagnostic Pvt ltd', 'Mehsana', NULL, NULL, '2021/02/08 14:59:04', 0, 'drytmanglam@gmail.com', 0, '9ea7a1192f1f86e97fd70d74f5778e4f'),
(139, 0, 1, 0, 0, 'Reena ', 'Rajan ', '7022530787', 'Manipal hospital ', 'Bangalore ', NULL, NULL, '2021/02/08 17:17:19', 0, 'C.reenarajan@gmail.com', 0, 'd1cc4c444eeb54bf24421bea2b4cb19c'),
(140, 0, 0, 0, 1, 'M S', 'GUPTA', '9415031335', 'Mediscan', 'Jhansi', NULL, NULL, '2021/02/08 18:16:39', 0, 'madhusudhan.gupta@rediffmail.com', 0, '70baca061d5d9b56443a5a79c0031b6f'),
(141, 0, 0, 1, 0, 'Kamal ', 'Chaudhry', '9988889852', 'SHPL', 'Chandigarh', '2021/02/12 16:51:22', '2021/02/12 17:52:03', '2021/02/08 20:07:41', 0, 'kamal.chaudhry@siemens-healthineers.com', 0, '6b434bf74d49ea3b58645555af645691'),
(142, 0, 0, 0, 1, 'Haresh', 'Vagdoda', '09998090852', 'MAVAJAT hospital ', 'Palanpur', '2021/02/12 13:50:18', '2021/02/12 13:51:18', '2021/02/08 21:41:01', 0, 'hareshvagdoda@gmail.com', 0, '5a7c10522987903be9185b804f783ff3'),
(143, 0, 0, 0, 1, 'Renuka', 'G', '9895328483', 'RCC', 'Trivandrum', '2021/02/12 14:30:33', '2021/02/12 15:30:50', '2021/02/09 06:46:03', 0, 'drrenukag@gmail.com', 0, '0ae5585b0e2c2138cf2cc46685248566'),
(144, 0, 0, 1, 0, 'Utal', 'Deka', '6002404987', 'Nemcare Hospital Pvt.Ltd', 'Guwahati', NULL, NULL, '2021/02/09 12:57:56', 0, 'utpaldeka474@gmail.com', 0, 'dc13306c621e4069dcb4b8c858d5f168'),
(145, 0, 0, 1, 0, 'Sumit', 'Das', '8723059655', 'Nemcare Hospital Pvt.Ltd', 'Guwahti', NULL, NULL, '2021/02/09 12:59:34', 0, 'sumit.das.7549@gmail.com', 0, '9287a13fa7b4ba21fb0a6f487cbcd636'),
(146, 0, 0, 1, 0, 'Ranjan', 'Sarma', '+918011629852', 'Primus Imaging', 'Guwahati', NULL, NULL, '2021/02/09 13:37:21', 0, 'ranjansarma1978@gmail.com', 0, 'd523d32b7a710839696ccf4d7f001594'),
(147, 0, 0, 1, 0, 'Surajkumar ', 'Chandrasekharan ', '9500088843', 'SHPL ', 'Chennai', NULL, NULL, '2021/02/09 13:52:30', 0, 'c.suraj_kumar@siemens-healthineers.com', 0, '4833068d18cb29a3c840a2322d352b9f'),
(148, 0, 0, 0, 1, 'Pratul Kumar', 'Sarma', '9864067648', 'Primus Imaging', 'Guwahati', NULL, NULL, '2021/02/09 14:00:23', 0, 'drpratulsarma@yahoo.co.in', 0, 'e6f32e3f111e2ed0105d030c58ac3676'),
(149, 0, 0, 0, 1, 'H.S', 'Das', '+919864067142', 'Matrix Diagnostics', 'Guwahati', NULL, NULL, '2021/02/09 14:03:32', 0, 'drhsdas@gmail.com', 0, '0af09c51c0b07ca97a11175d3ca00e72'),
(150, 0, 0, 0, 1, 'Partha', 'Hazarika', '+917002352636', 'Matrix Diagnostics', 'Guwahati', NULL, NULL, '2021/02/09 14:06:14', 0, 'drparthahazarika@yahoo.co.in', 0, 'ee57cefb684b8d64b8c4f3a19fbc838b'),
(151, 0, 0, 0, 1, 'J ', 'Debnath', '+917002588038', 'Ultralab', 'Nagaon ', NULL, NULL, '2021/02/09 14:09:02', 0, 'drjaganath7@gmail.com', 0, '9838696677aa938d1a2834ec7c5eb8da'),
(152, 0, 0, 0, 1, 'Phalguni', 'Das', '8638316040', 'Primus Imaging', 'Guwahati', '2021/02/12 13:00:39', '2021/02/12 13:01:39', '2021/02/09 14:22:21', 0, 'drphalgunidas@yahoo.co.in', 0, '86c45660971edd78e9ed47288035f707'),
(153, 0, 0, 0, 1, 'Rabin ', 'Saikia', '9435034379', 'Insight', 'Guwahati', NULL, NULL, '2021/02/09 14:59:56', 0, 'radio_1991rabin@yahoo.com', 0, '25b61a94d29006cd12620b643a7407e5'),
(154, 0, 0, 1, 0, 'Raja ', 'Roy', '9706077580', 'Healthcare', 'Guwahati', NULL, NULL, '2021/02/09 15:03:11', 0, 'rajaroy65@gmail.com', 0, '30051ca7c2815efaf3b852756477d5dd'),
(155, 0, 0, 0, 1, 'Tridib', 'Hazarika', '9435032490', 'Sanjivani hospital', 'Dibrugarh', NULL, NULL, '2021/02/09 15:38:43', 0, 'tridibhzrk@gmail.com', 0, '64932812323e1fe0659bf39b835c1c93'),
(156, 1, 0, 0, 0, 'Ramya ', 'Santhanam', '9841504000', 'Siemens ', 'Chennai', NULL, NULL, '2021/02/09 16:58:02', 0, 'ramyasanthanam@gmail.com', 0, 'acfb444edd6bc00d484e5a0bec46a568'),
(157, 0, 0, 1, 0, 'Abhinav ', 'Kaushik', '9840621483', 'Shpl', 'Chennai', '2021/02/12 16:31:20', '2021/02/12 16:33:38', '2021/02/09 17:51:40', 0, 'abhinav.kaushik@siemens-healthineers.com', 0, 'af8291ff934d7cbb52a0c257636be612'),
(158, 0, 0, 0, 1, 'Tajimal', 'Aboo', '9645887696', 'Aster MIMS', 'Calicut ', NULL, NULL, '2021/02/09 18:48:42', 0, 'tajimal@me.com', 0, '0fabe7593e8440613f4b7f517d60c60a'),
(159, 0, 0, 0, 1, 'Amruta', 'Mohan', '9446480705', 'Govt medical College kozhikode', 'Kozhikode', NULL, NULL, '2021/02/09 19:08:02', 0, 'amruthamazha@gmail.com', 0, 'baf4e0150d43dda5e09cd8e59490ad66'),
(160, 0, 0, 0, 0, 'Hanan ', 'Hameed ', '9645401672', 'Calicut medical college ', 'Calicut', NULL, NULL, '2021/02/09 19:21:26', 0, 'hananshahin261216@gmail.com', 0, '62368ae4a4c6f45bf77f98853ad32eb2'),
(161, 0, 0, 0, 1, 'Jaswinder', 'Singh', '09855778888', 'Raja hospital', 'Nawan Shahr', NULL, NULL, '2021/02/09 20:51:52', 0, 'jaswinder.rajahospital@gmail.com', 0, '7813ade6597107c2611c33ef9b98a335'),
(162, 0, 1, 0, 0, 'Shalini', 'Kumari', '8779293619', 'Siemens', 'Mumbai', NULL, NULL, '2021/02/10 08:33:30', 0, 'shalini.kumari@siemens-healthineers.com', 0, 'e88cfb794e17432013abf717f78fea9c'),
(163, 0, 0, 1, 0, 'ANTO RAMESH ', 'DELVI ', '9916509090', 'COLUMBIA ASIA HOSPITAL ', 'BANGALORE ', NULL, NULL, '2021/02/10 09:05:21', 0, 'anto.ramesh@columbiaindiahospitals.com', 0, 'c30e180473d3f19e9bfc355739e187fe'),
(164, 0, 0, 0, 0, 'Dr. Subita', 'Patil', '09867382703', 'Tata Memorial Centre', 'Mumbai', '2021/02/12 14:28:08', '2021/02/12 16:08:50', '2021/02/10 09:23:55', 0, 'psubita@gmail.com', 0, 'ca98b04509e84dd223728e35d7526df1'),
(165, 0, 0, 1, 0, 'suresh', 'nagalla', '+919550567860', 'Allengers', 'Hyderabad', NULL, NULL, '2021/02/10 09:47:55', 0, 'suresh.abhisht@gmail.com', 0, 'eb17d4f8081d0eb32bf5321d5fc170cb'),
(166, 0, 0, 1, 0, 'Aswin ', 'Vk', '09591165100', 'Siemens Healthcare Private Limited ', 'HYDERABAD TG', '2021/02/12 17:54:19', '2021/02/12 17:55:38', '2021/02/10 09:56:54', 0, 'aswin.vk@siemens-healthineers.com', 0, '285d18f7676c2e1c0016f11bdd832477'),
(167, 0, 0, 1, 0, 'Bipin', 'Kansakar', '+977 9849026576', 'East West  Concern P. ltd', 'Kathmandu', '2021/02/12 13:51:13', '2021/02/12 13:52:13', '2021/02/10 10:43:47', 0, 'radiology@ewc.com.np', 0, '2fb98c8f24791d5f40b62a111b6b86bd'),
(168, 1, 0, 0, 0, 'Sabita', 'Chalil', '9447541006', 'Medical college  calicut ', 'Calicut', NULL, NULL, '2021/02/10 12:31:01', 0, 'sabitachalil@gmail.com', 0, '829aeea11357c4978ca0d748c392b55a'),
(169, 0, 0, 0, 1, 'Girija', 'Ramachandran ', '9789117036', 'Royal care hospital ', 'Coimbatore ', NULL, NULL, '2021/02/10 12:59:00', 0, 'girija_ram_2000@yahoo.com', 0, 'fc336186033cb6d4c8920aa58e7c3d86'),
(170, 0, 0, 0, 1, 'Shiha', 'Mp', '9744704760', 'Dr', 'Calicut', NULL, NULL, '2021/02/10 12:59:16', 0, 'shihba.shihbamp@gmail.con', 0, '364b0207d59628c06b011ebeca4d9dec'),
(171, 0, 0, 0, 1, 'Dr.kavita', 'Vaishnav', '08160570127', 'AMC met medical college', 'Ahmedabad', '2021/02/12 14:23:26', '2021/02/12 15:27:50', '2021/02/10 12:59:37', 0, 'kavitaradio@gmail.com', 0, '307baf69a0caa02a320706d382e42b7f'),
(172, 0, 0, 0, 1, 'Sandeep', 'Prasad', '9008759432', 'Government medical College, kozhikode', 'Kozhikode', NULL, NULL, '2021/02/10 13:02:53', 0, 'sandeepprasad14@gmail.com', 0, '31ceb5e35742cc7b907913d143fb3b67'),
(173, 0, 0, 1, 0, 'shyam', 'shrestha', '+977-9851065987', 'East West Concern P. Ltd.', 'Kathmandu', '2021/02/12 12:18:13', '2021/02/12 12:19:13', '2021/02/10 13:06:19', 0, 'shyam.shrestha@ewc.com.np', 0, 'b6524715084c343c7d693f57cb3a3b3c'),
(174, 0, 0, 0, 1, 'Anita', 'Ohol', '9967297243', 'Curra hospital ghodbunder', 'Mumbai', NULL, NULL, '2021/02/10 13:06:54', 0, 'dr.anitaohol@gmail.com', 0, '5586e41b41e63fc956495461fd3e3985'),
(175, 0, 0, 0, 0, 'Binta.m', 'Binta.m', '9746590726', 'Hospital ', 'Calicut', NULL, NULL, '2021/02/10 13:07:54', 0, 'bintasarun@gmail.com', 0, '3d3923118fb4e5db32ab47e5ff9fa014'),
(176, 0, 0, 0, 1, 'Sabita', 'Desai', '09820078302', 'Breach Candy hospital', 'Mumbai', '2021/02/12 14:38:33', '2021/02/12 15:40:16', '2021/02/10 13:10:32', 0, 'sabbydesai@gmail.com', 0, 'a9aa7d2fb7d335061265dc3f999a0689'),
(177, 0, 0, 0, 1, 'Vinitha', 'Reddy', '09502077373', 'Breast center', 'VIJAYAWADA', NULL, NULL, '2021/02/10 13:16:37', 0, 'vinithakakarla@gmail.com', 0, 'c07d65ff5a07fe8b99efa927b7ccc463'),
(178, 0, 0, 0, 1, 'Mona', 'Mehta', '9820140461', 'Lilavati hospital ', 'Mumbai ', NULL, NULL, '2021/02/10 13:19:25', 0, 'drmonamehta@hotmail.com', 0, '95e840875bc403155f618f437768ca1d'),
(179, 0, 0, 0, 1, 'Aman', 'Daftary', '9987098846', 'Innovision imaging', 'Mumbai ', '2021/02/12 13:51:16', '2021/02/12 13:53:05', '2021/02/10 13:29:59', 0, 'aman.daftary@gmail.com', 0, '17779d821be5457c244e2358a30b25b6'),
(180, 0, 0, 0, 1, 'Nidhi', 'Doshi', '98194 17318 ', 'Picture this', 'Mumbai', '2021/02/12 16:49:30', '2021/02/12 16:50:30', '2021/02/10 13:38:23', 0, 'doshi.nidhi18@gmail.com', 0, 'b5f78d62f2ea8a29d9a3d870e13bd831'),
(181, 0, 0, 0, 1, 'AMAL', 'PARIKH', '09821121259', 'Aastha diagnostic centre ', 'Mumbai', NULL, NULL, '2021/02/10 13:43:30', 0, 'aasthadc@yahoo.com', 0, '8c58952e3e18df8a87458d8a9a55cb4e'),
(182, 0, 0, 0, 1, 'Pragya', 'Garg', '09891517700', 'Col pant Imaging Centre', 'New Delhi', NULL, NULL, '2021/02/10 14:32:21', 0, 'gargpragya2806@gmail.com', 0, '3900dc04f5e0f4a317085b052ccd6f68'),
(183, 1, 0, 0, 0, 'Babitha', 'M', '9496093766', 'Gov.Medical college, Kozhikode', 'Kozhikode', '2021/02/12 17:50:41', '2021/02/12 17:51:41', '2021/02/10 17:26:57', 0, 'babitabalakrishnan20@gmail.com', 0, 'e2dbd7ab50881d116245b1582583d596'),
(184, 0, 0, 0, 1, 'BALRAJ', 'GILL', '7838538644', 'Apollo Hospitalsâ€™s ', 'New Delhi', NULL, NULL, '2021/02/10 17:45:49', 0, 'cdrgill@gmail.com', 0, '1dbefeaf4a25d7e89518222124c0c0a1'),
(185, 0, 0, 0, 1, 'Aditi', 'Sonavane', '9619429955', 'Thane ultrasound centre', 'Thane', NULL, NULL, '2021/02/10 17:45:54', 0, 'aditi.c.doshi@gmail.com', 0, 'f66e3b9fe51937021950f250a3928196'),
(186, 0, 0, 0, 1, 'Sonal', 'Chauhan', '7021043760', 'HN Hospital', 'Mumbai', '2021/02/12 17:03:11', '2021/02/12 17:04:11', '2021/02/10 18:50:37', 0, 'sonalklub2@yahoo.com', 0, '6220811df86a25f5ecc88164ba00d0d6'),
(187, 1, 0, 0, 0, 'Juvaina ', 'P', '9946050257 ', 'Iria', 'Kozhikode ', NULL, NULL, '2021/02/10 20:39:26', 0, 'juvaina.faiz@gmail.com', 0, '72dda9c48e832ce6089febf93a4f4b1b'),
(188, 0, 0, 0, 1, 'Yojana', 'Nalawade', '09820034898', 'Nutan Mammography Centre', 'Mumbai', NULL, NULL, '2021/02/10 22:36:23', 0, 'Yojnalawade@gmail.coml', 0, '37a11587e91808d049b8409282944bac'),
(189, 0, 0, 0, 1, 'Monali', 'Warade', '08451938615', 'Saifee hospital Mumbai ', 'MUMBAI', '2021/02/12 14:13:51', '2021/02/12 14:39:06', '2021/02/11 01:15:14', 0, 'monali1.warade@gmail.com', 0, '75ca64eaed393af99cb4ed6ea8652420'),
(190, 0, 0, 0, 0, 'Usha', 'Kaul', '9811120190', 'Yesh clinic', 'Faridabad ', '2021/02/12 14:07:50', '2021/02/12 14:38:21', '2021/02/11 06:22:03', 0, 'ushakaul5@gmail.com', 0, '71e5118dd2cf345b4fcc59c67e03789f'),
(191, 0, 0, 0, 1, 'Aman', 'Priya', '9003926587', 'JIPMER, Puducherry', 'Puducherry', '2021/02/12 12:56:50', '2021/02/12 13:12:56', '2021/02/11 07:02:44', 0, 'amandpsbokaro@gmail.com', 0, 'f374fb6dee4cf451f078f664cdb23931'),
(192, 0, 0, 1, 0, 'Halim', 'Ansari', '7838066503', 'Siemens Healthcare', 'Ranchi', '2021/02/12 13:19:05', '2021/02/12 15:55:23', '2021/02/11 07:37:46', 0, 'halim.ansari88@hotmail.com', 0, '31467b9e6ff596118f914b5d91a20067'),
(193, 0, 0, 0, 1, 'Sharmin Rupa', 'Akhtar', '+880 1759916622', 'Popular Diag. , Ibn Sina Trust', 'Dhaka', NULL, NULL, '2021/02/11 08:03:04', 0, 'dr.sharminakhtarrupa@yahoo.com', 0, '92683cfd1bb6000b21c5ad12a08e5447'),
(194, 0, 0, 0, 1, 'Shoheli', 'Sultana', '+880 1711488800', 'Square Hospital ', 'Dhaka', NULL, NULL, '2021/02/11 08:05:54', 0, 'drshohelirad@gmail.com', 0, 'c9788de6acf2c700d249d7fc358398f4'),
(195, 0, 0, 0, 1, 'Fatema', 'Doza', '+880 1716778381', 'Popular Diagnostics, Shyamoli Branch', 'Dhaka', '2021/02/12 20:16:57', '2021/02/12 20:20:21', '2021/02/11 08:08:03', 0, 'fatemadoza@gmail.com', 0, 'ea6a5cfb92feed793b6e86bdbc41f061'),
(196, 0, 0, 0, 1, 'Subash ', 'Mazumder', '+880 1819329484', 'Chevron Clinical Laboratory (pte.) Ltd.', 'Chittagong', NULL, NULL, '2021/02/11 08:12:55', 0, 'subashmazumder@gmail.com', 0, 'dab33e13ac77e9a69cdff83955857a4f'),
(197, 0, 0, 0, 1, 'Justin', 'Clump', '+880 1711883466', 'Ibn Sina Diag. ', 'Chittagong', NULL, NULL, '2021/02/11 08:15:26', 0, 'justincmc43rd@gmail.com', 0, 'f19ea52f363ebea44ffec04038a379dc'),
(198, 0, 0, 1, 0, 'Sudip', 'Thapa', '9851123263', 'EWC', 'Kathmandu', '2021/02/12 16:52:36', '2021/02/12 17:47:46', '2021/02/11 09:37:45', 0, 'sudip.thapa@ewc.com.np', 0, '3be1ac1389a8372c7097421c3af7fce6'),
(199, 0, 0, 0, 1, 'Paresh ', 'Sukhani', '09829116070', 'Mahatma Gandhi Medical College', 'Jaipur', NULL, NULL, '2021/02/11 09:39:40', 0, 'pareshsukhani@gmail.com', 0, '03957c63e863f59dd0502ff02fb59b6b'),
(200, 0, 0, 0, 1, 'Aanand', 'Binnani', '9414142747', 'Binnani Diagnostic Center', 'Bikaner', NULL, NULL, '2021/02/11 09:43:06', 0, 'a_binnani@yahoo.co.in', 0, 'f3f878b4708a1919ea4588f711040d3e'),
(201, 0, 0, 0, 1, 'Dr Chandrika ', 'Sahu', '09827129090', 'Sahu Diagnostic Center', 'Raipur', NULL, NULL, '2021/02/11 09:44:07', 0, 'sahudiagnostics@gmail.com', 0, 'bc0841029a251c8fd94bc183d024e68d'),
(202, 0, 0, 0, 1, 'Kumar', 'Debashish', '9993032999', 'Manya Imaging Point', 'Bilaspur', NULL, NULL, '2021/02/11 09:45:41', 0, 'swatidevashish@gmail.com', 0, '59b7d9df608cf177a255310b11d8bbc6'),
(203, 0, 0, 0, 1, 'Neeteesh ', 'Gupta', '09116132227', 'Suryam Diagnostic Center', 'Jaipur', NULL, NULL, '2021/02/11 09:47:51', 0, 'drmiteshg@gmail.com', 0, '0d69b4843759f1840c0ae2b0093a9303'),
(204, 0, 0, 0, 1, 'Nikhil ', 'Bansal', '09610120123', 'Mahatma Gandhi Medical College', 'Jaipur', NULL, NULL, '2021/02/11 09:49:11', 0, 'nikhil.op.bansal@gmail.com', 0, '391b9c15e131a6ec45bc361a6c428b0e'),
(205, 0, 0, 0, 1, 'Hemant', 'Jangid', '9829024673', 'Satyam Diagnostic Center', 'Jodhpur', NULL, NULL, '2021/02/11 09:52:06', 0, 'dr.aju.jangid@gmail.com', 0, 'dd29fb05e72316d073e2c4b719cedee8'),
(206, 0, 0, 0, 1, 'Sanjay ', 'Nathani', '09462772596', 'Medi Pulse Hospital', 'Jodhpur', NULL, NULL, '2021/02/11 09:56:52', 0, 'drsanjaynathani2006@gmail.com', 0, 'fc106c35452b0fadfeec3f6d01598843'),
(207, 0, 0, 0, 1, 'Tripty ', 'Shah', '9352037799', 'Grayscale Interventions ', 'Jaipur', NULL, NULL, '2021/02/11 09:58:29', 0, 'triptichandra@gmail.com', 0, '9437fbd31e79668e6e7bd69a7aceee3f'),
(208, 0, 0, 0, 1, 'Shashank ', 'Kabra', '09829199199', 'Camberwell Diagnostics ', 'Jaipur', NULL, NULL, '2021/02/11 10:00:26', 0, 'dr_shashankkabra@yahoo.com', 0, 'e81c454f3c519798a494c26c0fe04e03'),
(209, 0, 0, 0, 1, 'A R ', 'Nagvekar', '07023389523', 'Rajasthan Hospital', 'Jaipur', NULL, NULL, '2021/02/11 10:01:48', 0, 'dr.amolnagvekar@gmail.com', 0, '76d1d505eadf5388c8cd3b14a8c54224'),
(210, 0, 0, 0, 1, 'Deepak', 'Aggarwal', '09582999597', 'Olive Diagnostic Center', 'Jaipur', NULL, NULL, '2021/02/11 10:03:52', 0, 'dragarwaldeepak18@gmail.com', 0, '972aaaa4356e722d06f3f178e0a505b9'),
(211, 0, 0, 0, 1, 'Manish', 'Rajpoot', '7729021111', 'Rajasthan Hospital', 'Jaipur', NULL, NULL, '2021/02/11 10:06:37', 0, 'infinityinetervention@gmail.com', 0, '6d8a1b3bd78f347d3e0f7d9146920759'),
(212, 0, 0, 0, 1, 'Arpit', 'Samdani', '9521818347', 'Freelancer ', 'Kota', NULL, NULL, '2021/02/11 10:10:25', 0, 'arpitsamdani@gmail.com', 0, '6ba89be56c3449b6eee014e1ad96bda4'),
(213, 0, 0, 1, 0, 'A K', 'Tripathi ', '9871886700', 'SHPL', 'Chandigarh ', NULL, NULL, '2021/02/11 10:12:02', 0, 'er.aktripathi@gmail.com', 0, 'e0e9ed82049bcb490f8211b60629dcc0'),
(214, 0, 0, 1, 0, 'Onkar', 'Tendulkar', '9930479711', 'Siemens Healthcare Private Limited', 'Pune', NULL, NULL, '2021/02/11 10:27:41', 0, 'onkar.tendulkar@siemens-healthineers.com', 0, '0cbe963b58c5adb43df880684e0a9ad0'),
(215, 0, 0, 0, 1, 'Anup', 'Kurele', '8225095234', 'Nidaan Imaging Center', 'Bhuj', NULL, NULL, '2021/02/11 10:36:01', 0, 'nidaanradiology@gmail.com', 0, '904e10724b67038e35af67e0ba86e4a5'),
(216, 0, 0, 0, 1, 'Karuna', 'Agawane', '7666935335', 'Ruby Ailcare', 'Mumbai', NULL, NULL, '2021/02/11 10:37:09', 0, 'agawanedkaruna@gmail.com', 0, '3b66e218927c0bb3cdf87306b2988da4'),
(217, 0, 0, 0, 1, 'Ankita', 'Morakhia', '8009530972', 'Morakhia imaging center', 'Gandhidham', NULL, NULL, '2021/02/11 10:37:57', 0, 'drankita.radio@gmail.com', 0, 'a445f23f10b30b17f49c8d058f09bea5'),
(218, 0, 0, 0, 1, 'Towhida', 'Khan', '+880 1718065835', 'CMH (Combined Military Hosp)', 'Dhaka', NULL, NULL, '2021/02/11 10:43:04', 0, 'towhidakhan@gmail.com', 0, '7acfc05b6a468f0b0db48a3bd3f1308e'),
(219, 0, 0, 1, 0, 'Ankur', 'Kapoor', '9501001605', 'Siemens Healthineers', 'Lucknow', NULL, NULL, '2021/02/11 10:53:20', 0, 'kapoor.ankur@siemens-healthineers.com', 0, '2733b21d5bf490a74b11fe3b72478f28'),
(220, 0, 0, 1, 0, 'Pradeep Kumar', 'Yadav', '06305453695', 'Software', 'hyderabad', NULL, NULL, '2021/02/11 10:58:51', 0, 'pradeepkumar.yadav@ceremorphic.com', 0, '5a6c1daebd7343dba77580d134080297'),
(221, 0, 0, 1, 0, 'Hassan', 'Mohamed', '8008119928', 'Siemens ', 'Chennai', '2021/02/12 13:03:40', '2021/02/12 15:15:28', '2021/02/11 13:25:30', 0, 'hassan.mohamed@siemens-healthineers.com', 0, '6a763d27358e38cd09deb023c43bf02b'),
(222, 0, 1, 0, 0, 'Pooja', 'Muddana', '8940834705', 'Jipmer ', 'Pondicherry ', NULL, NULL, '2021/02/11 13:39:45', 0, 'poojamailsu1@gmail.com', 0, '57dc86e7896083f8a3bb8405d550ed09'),
(223, 0, 1, 0, 0, 'Savita', 'Gawand', '9860878689', 'Vinchurkar Diagnostics Pvt Ltd', 'Nasik(Maharashtra)', '2021/02/12 17:16:24', '2021/02/12 17:54:09', '2021/02/11 13:51:57', 0, 'savitag1586@gmail.com', 0, '4b32feec61359c1eca118a981ce987bb'),
(224, 0, 0, 1, 0, 'Lokesh', 'Agrawal', '7869916478', 'Siemens Healthcare Pvt. Ltd.', 'Indore ', NULL, NULL, '2021/02/11 13:52:37', 0, 'lokesh.agarwal@siemens-healthineers.com', 0, '33b1328eff3717a9bfd52530f3cc15e9'),
(225, 0, 0, 0, 1, 'Anwar', 'Malek', '9725161456', 'Gurukul Hospital', 'Rajkot', NULL, NULL, '2021/02/11 14:08:22', 0, 'dranwarfm@rediff.com', 0, '564028bf3c06a91bb3b0d638488f9dd2'),
(226, 0, 0, 0, 1, 'Darshan', 'Bosamia', '9824083416', 'Darshan diagnostic', 'Bhuj', NULL, NULL, '2021/02/11 14:09:47', 0, 'drdarshboss@yahoo.co.in', 0, '8eed4cc459c98d13b062b6adfed518cd'),
(227, 0, 0, 0, 1, 'Deval', 'Chauhan', '9687633319', 'Dev Diagnostics', 'Amreli', NULL, NULL, '2021/02/11 14:10:54', 0, 'drdeval.chauhan716@gmail.com', 0, '5b995c949030d24c9fc74ad9b17df474'),
(228, 0, 0, 0, 1, 'Hardik', 'Parmar', '9409443944', 'Parmar X-ray sonography', 'Amreli', NULL, NULL, '2021/02/11 14:12:04', 0, 'drhardikparmar@gmail.com', 0, '6f60714bb86e85565805f61ed0aa973b'),
(229, 0, 0, 0, 1, 'Harshad', 'Rathod', '9879532500', 'Dr Harshad Rathod X-ray , Sonography and ct scan Center', 'Amreli', NULL, NULL, '2021/02/11 14:14:30', 0, 'dr_harshadrathod@yahoo.com', 0, '25dbf694a8c965e36695f4e4b1c21e50'),
(230, 0, 0, 0, 1, 'Jyoti', 'Sorathiya', '9978912439', 'Shreeji imaging center', 'Anjar', NULL, NULL, '2021/02/11 14:22:37', 0, 'shreejiimaging2014@gmail.com', 0, 'd772becb14f5070817f097f7e1fbe187'),
(231, 0, 0, 0, 1, 'Kamlesh', 'Darania', '9825855359', 'Suvidha imaging', 'Junagadh', NULL, NULL, '2021/02/11 14:23:49', 0, 'suvidhaimaging@yahoo.in', 0, '277f5a2c748c085df638bed6037bfeb9'),
(232, 0, 0, 0, 1, 'Kurang', 'Gandhi', '9374384564', 'Gandhi\'s Diagnostic center', 'Anjar', NULL, NULL, '2021/02/11 14:25:33', 0, 'kjgandhi7@yahoo.com', 0, '0d0b16a58b9e9c07161011d26382b721'),
(233, 0, 0, 0, 1, 'Manoj', 'Dholu', '7574014546', 'Satyam imaging center', 'Botad', NULL, NULL, '2021/02/11 14:27:00', 0, 'manojgdholu@gmail.com', 0, '6611c237aa3ab88b3359a5084c7418c0'),
(234, 0, 0, 0, 1, 'Paras', 'Patel', '9033320713', 'Apex sonography Center', 'Wankaner', NULL, NULL, '2021/02/11 14:28:34', 0, 'paraspatel1113@gmail.com', 0, '2f5580c3379e7b868c4dff41f013509c'),
(235, 0, 0, 0, 1, 'Priyansh ', 'Thacker', '9727737074', 'Prarthna imaging center', 'Gandhidham', NULL, NULL, '2021/02/11 14:30:19', 0, 'drpriyanshthacker@gmail.com', 0, '885849b491a1582ae2d106367d090b2d'),
(236, 0, 0, 0, 1, 'Ramgopal', 'Pansuriya', '6352032128', 'Pooja Diagnostic center', 'Gandhidham', NULL, NULL, '2021/02/11 14:31:30', 0, 'poojadiagnostickutch@gmail.com', 0, '090064beb1129be9f1e0c009e22427d4'),
(237, 0, 0, 0, 1, 'Jagdish', 'Sureja', '9825340593', 'Shivam X-ray and Sonography Clinic', 'Junagadh', NULL, NULL, '2021/02/11 14:33:28', 0, 'drjjsureja@yahoo.co.in', 0, '85a89d842e57082b81e12b88d0b839ff'),
(238, 0, 0, 0, 1, 'Jalpan', 'Rupapara', '9913378337', 'Aum imaging', 'Junagadh', NULL, NULL, '2021/02/11 14:35:53', 0, 'jalpan.gir@gmail.com', 0, '51cb9bfbbe698e9780833316bbcf44d1'),
(239, 0, 0, 0, 1, 'Pravin', 'Lakum', '9377734149', 'Swastik CT scan', 'Surendranagar', NULL, NULL, '2021/02/11 14:37:25', 0, 'pjlakum9@gmail.com', 0, '1d8dd26be2b0a422f307d665be1eeba8'),
(240, 0, 0, 0, 1, 'Sunil', 'Patel', '9879290088', 'Ami imaging center', 'Junagadh', NULL, NULL, '2021/02/11 14:38:32', 0, 'sunilpateljnd@gmail.com', 0, 'eb912cb2b6f4c8e92486b16ba9bc7a49'),
(241, 0, 0, 0, 1, 'Pushkar', 'Dabhi', '9558270177', 'Het Imaging', 'Jasdan', NULL, NULL, '2021/02/11 14:40:04', 0, 'drpushkar777@gmail.com', 0, '63a5f0ee947def5e46942a036a4a6aa7'),
(242, 0, 0, 0, 1, 'amitkumar', 'tiwari', '09617085184', 'Samarpan imaging solutiona', 'Indore', NULL, NULL, '2021/02/11 15:01:29', 0, 'dr.aamtiwari@gmail.com', 0, '190ba4de25651eae46455c4e69765106'),
(243, 0, 0, 0, 1, 'Vivek Ramesh Dadaga', 'Dadaga', '9819809544', 'KSHEMA', 'Mangalore', NULL, NULL, '2021/02/11 15:13:41', 0, 'dadgavivek@gmail.com', 0, 'a11bc056b19ab1b8f4dcfe30ef4bdd9d'),
(244, 0, 0, 0, 0, 'Dilan', 'Edwards', '+94773442397', 'Dimo pvt ltd', 'Colombo', '2021/02/12 13:12:36', '2021/02/12 17:53:01', '2021/02/11 15:19:56', 0, 'dilan.edwards@dimolanka.com', 0, '062806500ee69eec37d332dca5d651aa'),
(245, 0, 0, 1, 0, 'Sajith', 'Perera', '0766840778', 'DIMO', 'Colombo ', '2021/02/12 13:21:24', '2021/02/12 15:31:08', '2021/02/11 15:25:11', 0, 'sajith.perera@dimolanka.com', 0, '372176eb28c74d5c42870bb306a02265'),
(246, 0, 0, 0, 1, 'Shilpi', 'Gupta', '7049964864', 'Doctors', 'Indore', NULL, NULL, '2021/02/11 16:08:43', 0, 'shilpi.guptaji162@gmail.com', 0, 'd9c18567b203027cb900118f7dce0bf9'),
(247, 0, 0, 0, 1, 'Sakina', 'Kaydawala', '09765454864', 'Chrc', 'Indore', NULL, NULL, '2021/02/11 16:09:09', 0, 'dr.sakinahasan@gmail.com', 0, 'c8204bdc7054ad2aa6c4ad2a0bcdb72e'),
(248, 0, 0, 0, 1, 'Varun', 'P', '7406910250', 'KSHEMA', 'Mangalore', NULL, NULL, '2021/02/11 16:11:28', 0, 'vkcourse@gmail.com', 0, 'f10c2bafae6b18f3e2d4019afb817f3c'),
(249, 0, 0, 0, 1, 'bhawna', 'dev', '9840018243', 'SRIHER', 'CHENNAI', '2021/02/12 17:24:59', '2021/02/12 17:50:52', '2021/02/11 16:13:08', 0, 'bhawnadev@gmail.com', 0, '77082fd08319241e2e75a6644ed2f3c5'),
(250, 1, 0, 0, 0, 'Pratibha', 'Issar', '9407983540', 'JLN HOSPITAL AND RESEARCH CENTER', 'BHILAI', NULL, NULL, '2021/02/11 16:26:52', 0, 'mareesh_23@yahoo.co.in', 0, 'e26b0ab114c610c266da519bdc9d30d5'),
(251, 0, 0, 0, 1, 'Adithya', 'Salgado', '0718113344', 'Durdans hospital', 'Colombo 3 - Colpetty', NULL, NULL, '2021/02/11 16:33:24', 0, 'mnways@gmail.com', 0, '1ae9e90ac42bfd9e0c539b92982d88ca'),
(252, 0, 0, 0, 1, 'Aneri ', 'Thakkar', '8552037247', 'Ishan Imaging', 'Vadodara ', NULL, NULL, '2021/02/11 17:00:16', 0, 'dranerithakkar@yahoo.com', 0, 'd0281a6c2b2213703d7a6af958f3e85c');
INSERT INTO `tbl_users` (`id`, `ms`, `miss`, `mr`, `dr`, `fname`, `lname`, `mobile`, `organisation`, `city`, `login_date`, `logout_date`, `joining_date`, `logout_status`, `email`, `is_varified`, `password`) VALUES
(253, 0, 0, 1, 0, 'Umang', 'Shah', '9930900399', 'SHPL', 'Mumbai', NULL, NULL, '2021/02/11 17:02:42', 0, 'umang.shah@siemens-healthineers.com', 0, '4f818f23df5206d1a893c58ab12d0474'),
(254, 0, 0, 0, 1, 'Anamika', 'Jha', '9849694875', 'Institute of Medicine, TUTH, Kathmandu, Nepal', 'Kathmandu', '2021/02/12 15:27:27', '2021/02/12 15:28:27', '2021/02/11 18:16:33', 0, 'dranamikakasyap@gmail.com', 0, '6c1ae761deb496504df40d6da4d1e19f'),
(255, 0, 0, 0, 1, 'Ram ', 'Ghimire ', '9861033736', 'Nepal MEDICITI ', 'Kathmandu ', NULL, NULL, '2021/02/11 19:43:16', 0, 'r.ghimire@hotmail.com', 0, '93f3f916fb1a5468edf13e51845f750d'),
(256, 0, 1, 0, 0, 'Simran', 'Dua', '9729301387', 'Saims', 'Indore', NULL, NULL, '2021/02/11 19:50:08', 0, 'simrandua1995@gmail.com', 0, '16b4bc71be72f662a74310d8b057ee66'),
(257, 1, 0, 0, 0, 'Jaslin', 'Bawa', '09871880553', 'Siemens Healthineers', 'New Delhi', NULL, NULL, '2021/02/11 20:19:22', 0, 'jaslin.lamba@siemens-healthineers.com', 0, '4613b87c7ea4062c3628c811561e2231'),
(258, 0, 0, 0, 1, 'Sriluxayini ', 'Manikkavasakar ', '0777113480', 'Teaching hospital Jaffna ', 'Jaffna ', NULL, NULL, '2021/02/11 20:39:20', 0, 'msriluxayini@yahoo.com', 0, '6233c8d43b2748b2b0eff819ab9b4fe2'),
(259, 1, 0, 0, 0, 'Khushboo', 'Bhadani', '7208185374', 'Siemens Healthineers', 'Mumbai ', NULL, NULL, '2021/02/11 20:52:12', 0, 'khushi.bhadani@gmail.com', 0, '3a0f34bff856be14b3fde0f93795562f'),
(260, 0, 0, 0, 1, 'Mona', 'Shastri', '09879515556', 'SMIMER', 'SURAT', NULL, NULL, '2021/02/11 20:59:07', 0, 'monadigant@hotmail.com', 0, 'cf00af5b366a7038e9c583ef732b4480'),
(261, 0, 0, 0, 1, 'Ekta', 'Desai', '9825859399', 'SMIMER hospital ', 'Surat', '2021/02/12 14:23:59', '2021/02/12 15:19:53', '2021/02/11 21:03:51', 0, 'ektadesaishah@gmail.com', 0, '5b42fb702b0742f35c157e859b3341a6'),
(262, 0, 0, 0, 1, 'Monica', 'Mehrotra', '09161626000', 'Mehrotra Diagnostics', 'KANPUR', NULL, NULL, '2021/02/11 21:17:07', 0, 'monicaradiology@rediffmail.com', 0, 'f38099d1ef56db7f54a5b208e118ea9a'),
(263, 0, 0, 1, 0, 'Girraj', 'Chokotiya', '8770105088', 'Shri arbindo medical College ', 'Indore', NULL, NULL, '2021/02/11 21:29:10', 0, 'chokotiagirraj@gmail.com', 0, '7169c8b3cedca09513665ac4a04db362'),
(264, 0, 0, 0, 1, 'Nipa', 'Patidar', '9904266646', 'Smimer hospital', 'Surat', NULL, NULL, '2021/02/11 21:43:14', 0, 'docnipa011@gmsil.com', 0, '878ef4f2501019a73ebe575f2dd211f9'),
(265, 0, 0, 0, 1, 'Nipa', 'Patidar', '9904266646', 'Smimer hospital', 'Surat', NULL, NULL, '2021/02/11 21:44:14', 0, 'docnipa01151974@gmail.com', 0, 'd82cd90800672754cf819c661986fbd8'),
(266, 0, 0, 0, 1, 'Nipa', 'Patidar', '9904266646', 'Smimer hospital', 'Surat', NULL, NULL, '2021/02/11 21:46:00', 0, 'docnipa011@gmail.com', 0, '730793aa1342bde0546fbe9567ad03e7'),
(267, 0, 0, 0, 1, 'Sultana', 'Parvin', '+880 1769038079', 'CMH (Combined Military Hospital)', 'Dhaka', '2021/02/12 14:07:20', '2021/02/12 14:36:36', '2021/02/11 22:35:44', 0, 'dr.sultanaparvin@yahoo.com', 0, '9f9490361300a722e003d0aeaec2d3b2'),
(268, 0, 0, 0, 1, 'Uday singh', 'Sengar', '7000048925', 'SAIMS Indore ', 'Indore ', NULL, NULL, '2021/02/12 00:51:12', 0, 'drudaysinghsengarjio1990@gmail.com', 0, '8ceb4b5bc4eee1e677c62dbef00895f2'),
(269, 0, 0, 0, 0, 'Santosh', 'Dixit', '9850245490', 'PCCM', 'Pune', NULL, NULL, '2021/02/12 03:49:11', 0, 'sgdixit@gmail.com', 0, 'cf31a50f7e7b39a44c39d1842d130a6e'),
(270, 0, 0, 0, 1, 'Kanchan', 'Sadhwani', '09820634292', 'Suburban Diagnostics', 'Mumbai', NULL, NULL, '2021/02/12 06:59:43', 0, 'kanchantul@gmail.com', 0, '74c17fdc67c98ca10fe961fa1c40fa99'),
(271, 0, 0, 0, 1, 'Niketa', 'Chotai', '96547812', 'RadLink', 'SINGAPORE', '2021/02/12 14:20:14', '2021/02/12 14:21:14', '2021/02/12 07:01:35', 0, 'Niketachotai@gmail.com', 0, '303aa1d5513c83e31253f5ff28808816'),
(272, 1, 0, 0, 0, 'Varsha', 'Hardas', '9822326486', 'Rubyhall clinic ', 'Pune ', '2021/02/12 15:06:34', '2021/02/12 16:11:45', '2021/02/12 07:35:27', 0, 'varshardas@hotmail.com', 0, '56344fb67c96c05d03435d6ae9cc4bbe'),
(273, 0, 0, 0, 1, 'Raghvendra', 'Singh', '08935974727', 'Rims ranchi', 'Ranchi', NULL, NULL, '2021/02/12 07:45:48', 0, 'raghav.krazzy@gmail.com', 0, '3c2804a41065292906c0ec612bd1252e'),
(274, 0, 0, 0, 1, 'Vaneeta', 'Kapur', '9810115145', 'Pant diagnostics pvt ltd', 'Delhi', '2021/02/12 14:02:01', '2021/02/12 14:38:07', '2021/02/12 07:53:41', 0, 'vaneeta.kapur@gmail.com', 0, 'e96fedd6e4fbada0925c23d2aeeef81b'),
(275, 0, 0, 0, 1, 'Dhara', 'Shah', '9426755815', 'Echoes sonography and mammography center ', 'Surat ', '2021/02/12 14:56:18', '2021/02/12 15:31:21', '2021/02/12 07:59:31', 0, 'drdharadishant@gmail.com', 0, '92a2608865178f7cec9629996a19a0c0'),
(276, 0, 0, 0, 1, 'Shobana', 'Thangam ', '09444389056', 'Gemini Scans ', 'Chennai', NULL, NULL, '2021/02/12 08:15:43', 0, 'gowthamsairam24@gmail.com', 0, 'c29b6ac2d04091f1c3eca90186413aec'),
(277, 0, 0, 0, 1, 'Hanisha', 'Gwalani', '9860869496', 'Shivkala diagnostic centre', 'Ulhasnagar', NULL, NULL, '2021/02/12 08:15:52', 0, 'drhanishag@gmail.com', 0, '16d0da6f9e4ad7b57e4524521e6e29bc'),
(278, 0, 0, 0, 1, 'Dr Rajni', 'Saxena', '09811907064', 'Fortis', 'New Delhi', NULL, NULL, '2021/02/12 08:19:37', 0, 'rajnisaxenads@gmail.com', 0, '31546a1dc2e891b2ac6576df3a3da15c'),
(279, 0, 0, 1, 0, 'Animesh ', 'Joshi ', '8800693434', 'Healthcare Devices and Informatics', 'Pune', NULL, NULL, '2021/02/12 08:21:57', 0, 'animeshjosh@gmail.com', 0, 'ba0bbc0c6d18ac3a9dea4d99c1628da5'),
(280, 0, 0, 0, 1, 'Mangala', 'Targe', '7798371990', 'HCG manavata cancer hospital', 'Nashik', '2021/02/12 15:30:37', '2021/02/12 15:52:21', '2021/02/12 08:28:55', 0, 'mangalahcg2018@gmail.com', 0, '1132244d506adbb150ba071f9cbfbbc9'),
(281, 1, 0, 0, 0, 'Shirisha', 'Jakka', '9701398484', 'Century super speciality hospital ', 'Hyderabad ', '2021/02/12 13:56:35', '2021/02/12 14:22:38', '2021/02/12 08:43:33', 0, 'shirisha.jakka@gmail.com', 0, '4e6761e28b9490f4901b06f938a4b130'),
(282, 0, 0, 0, 1, 'Saman', 'Chandana', '0717079539', 'Health', 'Kandy', '2021/02/12 13:28:46', '2021/02/12 15:24:41', '2021/02/12 08:45:05', 0, 'dr.saman1975@gmail.com', 0, '5d3bacb3250189001e2dfe071b24ac01'),
(283, 0, 0, 0, 1, 'shalini', 'jain', '9717411115', 'Dr Doda\'s Diagnostic Center', 'New Delhi', NULL, NULL, '2021/02/12 08:54:46', 0, 'drsjain71@gmail.com', 0, 'f115ce1a77061faff71066399873d8a3'),
(284, 0, 0, 0, 0, 'Veenu ', 'SINGLA ', '7087009362', 'Pgi ', 'Chandigarh ', '2021/02/12 14:49:08', '2021/02/12 17:53:41', '2021/02/12 08:59:54', 0, 'veenupgi@gmail.com', 0, '6ca5e1624234e24f6c04b33f85dd46e6'),
(285, 0, 0, 0, 1, 'Nipa', 'Patidar', '9904266646', 'Smimer hospital', 'Surat', NULL, NULL, '2021/02/12 09:02:12', 0, 'docnipa0115@gmail.com', 0, '209c6f30ba3d376947e4b2b833fc208f'),
(286, 0, 0, 0, 1, 'ANJANA', 'AGGARWAL ', '9810960935', 'MAHAJAN IMAGING CENTER', 'NEW DELHI', NULL, NULL, '2021/02/12 09:02:13', 0, 'anjanaa928@gmail.com', 0, 'b478401fd0be19547a4705651b34fd8f'),
(287, 0, 0, 0, 1, 'Padmesh', 'Jain', '08097450290', 'Namokaar diagnostic centre', 'Mumbai', NULL, NULL, '2021/02/12 09:02:17', 0, 'drpadmeshradio@gmail.com', 0, 'f4d39e9e2083780c6f5fb867ed41cafe'),
(288, 0, 0, 0, 1, 'kanchan', 'Bendale', '09405665080', 'Manav seva', 'Jalgaon', NULL, NULL, '2021/02/12 09:02:24', 0, 'kanchanbendale@yahoo.com', 0, '6fcd1b97ac98d9341ce480333f349c66'),
(289, 0, 0, 0, 1, 'ANJANA', 'AGGARWAL ', '9810960935', 'MAHAJAN IMAGINING CENTER', 'NEW DELHI', NULL, NULL, '2021/02/12 09:03:13', 0, 'anjanaa938@gmail.com', 0, '84327402754d34d250c94f66d0205df5'),
(290, 0, 0, 0, 1, 'Anil', 'Bansal', '9414401608', 'SONO Lab', 'Udaipur', NULL, NULL, '2021/02/12 09:12:30', 0, 'sonolab0294@gmail.com', 0, '8d954873a5c583f16fd9c229ea47d9b6'),
(291, 0, 0, 0, 1, 'Gulab', 'Chajer', '9486078843', 'Kushal Diagnostics', 'Sumerpur', NULL, NULL, '2021/02/12 09:15:08', 0, 'scanradiologist@gmail.com', 0, 'a04c7f8c2e7aec6c4bc0711b3c701bb0'),
(292, 0, 0, 0, 1, 'Shailendra', 'Luhadiya', '9413517136', 'Arihant Hospital', 'Bhilwara', NULL, NULL, '2021/02/12 09:16:59', 0, '9413517136dr@gmail.com', 0, '79e0f13c87e94032d37c5577257c1605'),
(293, 0, 0, 0, 1, 'Subhash', 'Tailor', '9001301144', 'Sonography Clinic', 'Bhilwara', NULL, NULL, '2021/02/12 09:19:08', 0, 'dr.subhashtailor@gmail.com', 0, '51f7c66e5672f2152bf78f04dcf15630'),
(294, 0, 0, 0, 1, 'akilesh', 'Patel', '09558062662', 'Gcri', 'Ahmedabad', NULL, NULL, '2021/02/12 09:19:23', 0, 'akileshpatel67@gmail.com', 0, '2ddb4adfdeccab9fae1cb03e9a97c3d9'),
(295, 0, 0, 0, 1, 'Ujwala', 'Jadhav', '9823060254', 'Nidan Sonography And X Ray Clinic', 'Satara', '2021/02/12 13:24:56', '2021/02/12 16:25:03', '2021/02/12 09:20:01', 0, 'dr_cuaj@yahoo.co.in', 0, '8b386670afa08f96c8d30e66bf463852'),
(296, 0, 0, 0, 1, 'Tapendra ', 'Tiwari', '9672922799', 'PIMS', 'Udaipur', NULL, NULL, '2021/02/12 09:21:36', 0, 'tapendratiwari121@gmail.com', 0, '11dd90134dc2d60334e6e9c6fa81d2d7'),
(297, 0, 0, 0, 1, 'Rambir', 'Singh', '9414934724', 'GBH American Hospital', 'Udaipur', NULL, NULL, '2021/02/12 09:23:39', 0, 'rambirdr@yahoo.in', 0, 'c4ecc83210f9958359be79cba44731c6'),
(298, 0, 0, 0, 1, 'Manish', 'Seth', '9413851031', 'Medicenter ', 'Udaipur', NULL, NULL, '2021/02/12 09:26:58', 0, 'drsethmanish@gmail.com', 0, '6e9c92fe56b3330efa74ad021e2d7d44'),
(299, 0, 0, 0, 1, 'Ramesh', 'Malav', '7891196111', 'Sion Sonography', 'Kota', NULL, NULL, '2021/02/12 09:28:24', 0, 'sionsonograpy@gmail.com', 0, '57c02f3585f82394dc3396b1fa413621'),
(300, 0, 0, 0, 1, 'Vrushali ', 'Deshpande', '09881736360', 'shalaka imaging centre', 'AURANGABAD', NULL, NULL, '2021/02/12 09:28:48', 0, 'vrushaliaun@gmail.com', 0, 'cba255e36874c9c32eacd826492d7831'),
(301, 0, 0, 0, 1, 'Gayatri', 'Senapathy', '9849417665', 'AIG hospitals', 'HYDERABAD', NULL, NULL, '2021/02/12 09:30:59', 0, 'sgayatri0311@gmail.com', 0, '6dc456aa1452f32d6fcf22e9b3538687'),
(302, 0, 0, 0, 1, 'Raghukul ', 'Tilak', '9352899500', 'GBH American', 'Udaipur', NULL, NULL, '2021/02/12 09:31:25', 0, 'drraghukultilak@gmail.com', 0, 'f403ce07b57d4b0bcab8d91c549f6f1a'),
(303, 0, 0, 0, 1, 'prachi', 'Shah', '9769661990', 'Picture this by  Jankharia ', 'Mumbai', NULL, NULL, '2021/02/12 10:07:49', 0, 'shahprachi62@gmail.com', 0, '8a01eb6ddcdb49374572f4847b09cf0c'),
(304, 0, 0, 1, 0, 'Narendra ', 'Wagh ', '09970327111', 'Vinchurkar  Diagnostic ', 'Nasik(Maharashtra)', '2021/02/12 15:37:32', '2021/02/12 15:51:15', '2021/02/12 10:16:50', 0, 'narenyw@gmail.com', 0, '4d589787847f77cb61987b745c74a2ce'),
(305, 1, 0, 0, 0, 'Rohini', 'Unde', '7798124616', 'Prashanti Cancer care mission', 'Pune', '2021/02/12 16:48:43', '2021/02/12 16:53:54', '2021/02/12 10:32:23', 0, 'rohiniunde85@gmail.com', 0, 'c9727bf22b5c32506a9a50781bb0945e'),
(306, 0, 0, 0, 0, 'Manjulika ', 'Dheerasekara ', '+94718564210', 'Apeksha hospital ', 'Colomb ', NULL, NULL, '2021/02/12 10:53:12', 0, 'prasanthi.cim@gmail.com', 0, '9d0886d26ef4e5afab306a7438231a57'),
(307, 0, 0, 0, 1, 'Aruna', 'Pallewatte', '0718385832', 'National hospital', 'Colombo', NULL, NULL, '2021/02/12 10:53:54', 0, 'asp31263@hotmail.com', 0, '7edd501ccb964887fec6f52831f86777'),
(308, 0, 1, 0, 0, 'Deepika', 'Sharma', '805433073', 'Govt medical college and hospital sec 33', 'Chandigarh', NULL, NULL, '2021/02/12 11:10:29', 0, 'angrish.deepika001@gmail.com', 0, 'c058d94b1d362027d83d088dd60d56bf'),
(309, 0, 0, 0, 1, 'anupam', 'jhobta', '09418100180', 'IGMC shimla', 'Shimla', NULL, NULL, '2021/02/12 11:14:57', 0, 'anupamjhobta@gmail.com', 0, '6173b6f5427245b5f211ea604494aa11'),
(310, 0, 1, 0, 0, 'Kanu', 'Priya', '94650337766', 'Student', 'Chandigarh', NULL, NULL, '2021/02/12 11:15:19', 0, 'kgahlot1999@gmail.com', 0, '0f3c2d72241bf329fd9bdf4721a96282'),
(311, 0, 0, 0, 1, 'Vijaykumar', 'Gupta', '9824311959', 'Rajkot Cancer Society', 'Rajkot', '2021/02/12 13:18:04', '2021/02/12 13:19:04', '2021/02/12 11:17:32', 0, 'drvkgupta2018@gmail.com', 0, '53050a44da1d540f6062642e8a10a3e2'),
(312, 0, 0, 0, 1, 'Ritu', 'Sharma', '08570815664', 'Radiology', 'Shimla', NULL, NULL, '2021/02/12 11:17:40', 0, 'sharmaritu368k12@gmail.com', 0, 'c8ae2b9052d4a0691491fcb429060d23'),
(313, 0, 0, 0, 1, 'Shruti', 'Thakur', '09418500500', 'Government ', 'Shimla', NULL, NULL, '2021/02/12 11:20:49', 0, 'tshruti878@yahoo.in', 0, '378d7c2915b016da131b8c520a2544c4'),
(314, 0, 1, 0, 0, 'Sai ', 'Kalyani', '8837574471', 'IGMC', 'Shimla', NULL, NULL, '2021/02/12 11:25:58', 0, 'saikalyani121@gmail.com', 0, 'a12f7e7b634b32388649e2113d2c08d7'),
(315, 0, 0, 0, 1, 'Rupa', 'R', '9789490023', 'Kmch', 'Coimbatore ', '2021/02/12 14:44:00', '2021/02/12 14:45:00', '2021/02/12 11:29:40', 0, 'drrrupa@gmail.com', 0, '302245cebbcd7193d0439b1688743015'),
(316, 0, 1, 0, 0, 'Tanu', 'Chaudhry', '7837111890', 'Gmch32', 'Chandigarh', NULL, NULL, '2021/02/12 11:40:48', 0, 'tanuchaudhry1234@gmail.com', 0, '609fcd5edd74b63fbb473d5f5bd041b9'),
(317, 1, 0, 0, 0, 'Smitha', 'M', '9744001874', 'Medical college', 'Kozhikode ', NULL, NULL, '2021/02/12 11:42:22', 0, 'sumir.niran@gmail.com', 0, '9fe829d59c3fd808f4eb91b0922655a9'),
(318, 0, 0, 0, 1, 'Anusha', 'P', '09148865272', 'HCG', 'Bangalore', NULL, NULL, '2021/02/12 11:44:38', 0, 'anushap10@gmail.com', 0, '77ced9c7d2c7e48d302c24a32b2d3358'),
(319, 0, 0, 1, 0, 'Chackochan ', 'PT', '8921555927', 'Siemens', 'Trichy', '2021/02/12 13:12:11', '2021/02/12 16:00:36', '2021/02/12 11:51:27', 0, 'chackochanpt@hotmail.com', 0, 'a868c7a4d5e23ef3f8ef53e7ce44461f'),
(320, 0, 0, 0, 0, 'Srinivasan ', 'Sunder ', '9846129545', 'Igmc', 'Shimla', NULL, NULL, '2021/02/12 11:53:12', 0, 'srinivasan1985@hotmail.com', 0, '261d4e5e79d776cc05a7b593daf50f60'),
(321, 0, 0, 0, 1, 'Divya', 'Patel', '+919586795607', 'Bjmc', 'Ahmedabad', '2021/02/12 12:05:10', '2021/02/12 18:02:39', '2021/02/12 11:53:52', 0, 'divyasp137@gmail.com', 0, '3effa07777c64ff86488582bc24d8ee0'),
(322, 1, 0, 0, 0, 'Kartik', 'Iyer', '9819475816', 'Siemens Healthineers ', 'Mumbai', NULL, NULL, '2021/02/12 12:01:29', 0, 'iyer.kartik@siemens-healthineers.com', 0, '67333ef19f139035f25dc00dc4d278ca'),
(323, 0, 1, 0, 0, 'Nidhi', 'Rana', '9805951197', 'Radiology ', 'Shimla', NULL, NULL, '2021/02/12 12:05:18', 0, 'nidhirana1197@gmail.com', 0, 'c50a46e5be4056a3522dfa32aafeba70'),
(324, 0, 0, 0, 1, 'Neemesh', 'Lodh', '9967588005', 'Aims Hospital', 'Mumbai', NULL, NULL, '2021/02/12 12:07:09', 0, 'neemeshl@gmail.com', 0, '9622152720154169705347eea97db71b'),
(325, 0, 0, 1, 0, 'Sathish', 'B', '9944935423', 'Siemens Healthcare Pvt Ltd', 'Coimbatore', '2021/02/12 13:50:01', '2021/02/12 15:05:37', '2021/02/12 12:14:15', 0, 'sathish.b@siemens-healthineers.com', 0, 'e76a70d1c80e6c0a0de7d77769ada4e1'),
(326, 0, 1, 0, 0, 'kunmun', 'passi', '9971988400', 'siemens', 'delhi', NULL, NULL, '2021/02/12 12:19:46', 0, 'kunmun.passi@siemmens-healthineers.com', 0, '1c65ea9664af089a9a984e61cf5dd92a'),
(327, 0, 0, 0, 1, 'Dr Vishnu', 'Nair', '9447076657', 'TRIVANDRUM MEDICAL COLLEGE', 'TRIVANDRUM', NULL, NULL, '2021/02/12 12:23:20', 0, 'vichukutt@gmail.com', 0, '4750919385a40267bbf3d6c6ae5b86d9'),
(328, 0, 0, 1, 0, 'Reynold', 'Roy', '09611518064', 'Coact', 'Bangalore', NULL, NULL, '2021/02/12 12:24:59', 0, 'reynold@coact.co.in', 0, '1171a4ddc1370e2a6de63e3f65f2bfab'),
(329, 0, 0, 0, 0, 'abc', 'test', '0987654321', 'test', 'mumbai', '2021/02/12 12:36:18', '2021/02/12 12:38:02', '2021/02/12 12:26:21', 0, 'pooja93jaiswal@gmail.com', 0, '0f06d709c95653ed92a8e5d4b81f9e60'),
(330, 0, 1, 0, 0, 'Vaishali', 'Chugh', '9501346381', 'Gmch', 'Chandigarh, Chandigarh, Chandigarh, India', NULL, NULL, '2021/02/12 12:30:11', 0, 'Chughvaishali1@gmail.com', 0, '2de6e8e3e24d6a9067565b964e6bce50'),
(331, 0, 0, 0, 1, 'Somen', 'Chakravarthy', '9204058142', 'Tata Main Hospital', 'Jamshedpur ', NULL, NULL, '2021/02/12 12:33:28', 0, 'somen@tatasteel.com', 0, '6b25fad53c20c63e5a943139ebbecc2d'),
(332, 0, 1, 0, 0, 'Kunmun', 'Passi', '9971988400', 'Siemens Healthineers', 'Delhi', '2021/02/12 13:57:04', '2021/02/12 13:58:04', '2021/02/12 12:34:20', 0, 'kunmun.passi@siemens-healthineers.com', 0, 'd259311e9f263ee44b84acda96fa8927'),
(333, 0, 0, 0, 1, 'Vijay', 'Aggarwal', '7004574684', 'Trust diagnostic ', 'Ranchi', NULL, NULL, '2021/02/12 12:39:53', 0, 'viju_rdu@yahoo.in', 0, '9f9bf2c8cfd745ecbe24fbf705cb3337'),
(334, 0, 0, 0, 1, 'Sujata', 'Mitra', '7763807483', 'MTMH', 'Jamshedpur ', NULL, NULL, '2021/02/12 12:48:44', 0, 'sujata.mitra@tatasteel.com', 0, '076d210ce635a0e16a1efdcb12f1220b'),
(335, 0, 0, 0, 1, 'Indika', 'Ekanayake ', '+94718410860', 'Ministry of Health Sri Lanka', 'KURUNEGALA ', '2021/02/12 15:38:36', '2021/02/12 15:49:38', '2021/02/12 12:48:50', 0, 'indika1ekanayake@gmail.com', 0, '423bcb1ac9bd778d77a190f397034648'),
(336, 0, 0, 0, 1, 'Sanal', 'Kumar', '9562044616', 'Vmkv medical college', 'Salem', NULL, NULL, '2021/02/12 12:56:07', 0, 'sanalkumar841@gmail.com', 0, '04b2d90f03f0ad9b978996701e8def2e'),
(337, 0, 0, 0, 1, 'Nishi', 'Kant', '9731450424', 'Advance diagnostic', 'Ranchi', NULL, NULL, '2021/02/12 12:56:13', 0, 'nishikant_2804@yahoo.co.in', 0, 'e49002155437c23daa48189bfc52ec32'),
(338, 1, 0, 0, 0, 'Binta', 'Binta', '9746590726', 'Mch', 'Calicut', NULL, NULL, '2021/02/12 12:57:04', 0, 'sarunkiliyadi@gmail.com', 0, '37306cca6b77b0e28e00e86628ed91bd'),
(339, 0, 0, 1, 0, 'Yash', 'Hemrajani', '8817629558', 'Siemens Healthcare Private Limited', 'Guwahati', NULL, NULL, '2021/02/12 12:59:10', 0, 'yash_006_power@yahoo.com', 0, '2f380ec38f8d7f6840433aca63b687c8'),
(340, 0, 0, 1, 0, 'Vikas', 'Gourgonda', '9900106792', 'Siemens', 'Hyderabad', '2021/02/12 17:01:21', '2021/02/12 17:05:42', '2021/02/12 13:05:05', 0, 'vikas.gourgonda@siemens-healthineers.com', 0, '4f1b66bbb4e01f381cb6e1f390c0545d'),
(341, 0, 0, 0, 1, 'SAMIR', 'PATEL', '09819673131', 'SONO HUB IMAGING CENTRE', 'Mumbai', '2021/02/12 13:06:33', '2021/02/12 17:56:41', '2021/02/12 13:05:12', 0, 'drsampatel28@gmail.com', 0, '1b19ed8a5ac1356c7ad936a60041cc32'),
(342, 0, 0, 1, 0, 'Ramesh', 'Mandru', '9840066797', 'Siemens', 'Chennai', NULL, NULL, '2021/02/12 13:05:18', 0, 'ramesh.mandru@siemens.com', 0, 'd1d2d4cf5d0d10a5e7f7fb213b338f8e'),
(343, 0, 0, 1, 0, 'Ramesh', 'Mandru', '9840066797', 'Siemens', 'Chennai', '2021/02/12 13:07:23', '2021/02/12 17:47:05', '2021/02/12 13:06:23', 0, 'ramesh.mandru@siemens-healthineers.com', 0, '18cb2e23d6ccb8b238dbc9bf7a351a3e'),
(344, 0, 0, 1, 0, 'Pradyumn', 'Mishra', '9810953233', 'Siemens', 'Lucknow ', NULL, NULL, '2021/02/12 13:07:09', 0, 'misra.pradyumn@gmail.com', 0, '2f97bdaa6c786f2b01a226dd9d8dc072'),
(345, 0, 0, 1, 0, 'Laxmikant ', 'Tewari', '9646045729', 'GMCH', 'Chandigarh ', '2021/02/12 14:17:41', '2021/02/12 14:18:41', '2021/02/12 13:18:28', 0, 'laxmikant5729@gmail.com', 0, '2746b2f44493058f55e3662e0044b66d'),
(346, 0, 0, 0, 0, 'Mrs abha sharma', 'sharma', '7837119595', 'gmch sector 32 chandigarh', 'chandigarh', '2021/02/12 14:16:01', '2021/02/12 14:17:01', '2021/02/12 13:23:15', 0, 'abhagmchchd@gmail.com', 0, 'dc717976cc6566f555d5ebba76083859'),
(347, 0, 0, 0, 1, 'swetam', 'kumar', '9871977711', 'orchid Hospital', 'ranchi', NULL, NULL, '2021/02/12 13:25:52', 0, 'drshwetam@gmail.com', 0, 'bc5fadec9d25d0116cd3668c11dc4ebe'),
(348, 0, 0, 0, 1, 'Mukesh', 'Surya', '9418485088', 'IGMC Shimla', 'Shimla', '2021/02/12 13:35:46', '2021/02/12 14:00:42', '2021/02/12 13:26:43', 0, 'mukeshsurya27@yahoo.com', 0, 'a76d4826d795487ee86a9cf6de5f0e92'),
(349, 0, 0, 1, 0, 'naveen', 'paulose', '9847051959', 'shpl', 'kichu', '2021/02/12 13:50:58', '2021/02/12 14:48:32', '2021/02/12 13:36:26', 0, 'naveen.paulose@siemens-healthineers.com', 0, '2b53f79a6e139b84cbdc1d0c58c55804'),
(350, 1, 0, 0, 0, 'Huney', 'Pradhan', '9841570745', 'TU Teaching Hospital', 'Kathmandu', '2021/02/12 16:29:45', '2021/02/12 18:04:57', '2021/02/12 13:39:37', 0, 'huney.p@gmail.com', 0, '022b3010b3a6c4aa71806b5154ee2425'),
(351, 0, 0, 0, 1, 'Rupa', 'Ananthasivan', '9980156087', 'Manipalhospital ', 'Bangalore ', NULL, NULL, '2021/02/12 13:42:37', 0, 'rupanth@yahoo.com', 0, 'b05bd9f59ce2de51751a9dec339a9d5d'),
(352, 0, 0, 0, 1, 'Tajimal', 'Aboo', '9644887696', 'Aster MIMS ', 'Calicut ', '2021/02/12 13:46:20', '2021/02/12 14:12:01', '2021/02/12 13:45:15', 0, 'tajimal@gmail.com', 0, '3a595f57b2107adbf2d7e5a46b904f6f'),
(353, 0, 0, 0, 1, 'Sahil R ', 'Patel', '9898945412', 'Nidaan imaging center', 'MAHESANA', NULL, NULL, '2021/02/12 13:50:11', 0, 'sahilpatel111@gmail.com', 0, 'c1a8bc42c8a5729fa30f71aceb0d70ac'),
(354, 0, 0, 0, 1, 'Madhavi', 'Chandra', '9899227799', 'SGRH ', 'New Delhi', '2021/02/12 14:08:01', '2021/02/12 14:38:06', '2021/02/12 13:53:44', 0, 'madhavimohanchandra@gmail.com', 0, '83316f067bfb7f6628450bb6eb216aff'),
(355, 0, 0, 0, 1, 'Tanvi', 'Jakhi', '9833295816', 'Mammocare', 'Mumbai', NULL, NULL, '2021/02/12 13:58:17', 0, 'tanvijakhi@gmail.com', 0, '59463eb872c6d2d59bcae96c76a19474'),
(356, 0, 0, 0, 1, 'Surabhi', 'Vyas', '9911972323', 'AIIMS, ND', 'N Delhi', '2021/02/12 14:31:42', '2021/02/12 14:32:42', '2021/02/12 14:13:00', 0, 'surabhivyas22@gmail.com', 0, '2bf9559e19c06729aa86dfee0ed30d68'),
(357, 0, 0, 0, 1, 'Sapna ', 'Marda', '7702090101', 'Yashoda hospital ', 'Secunderabd ', '2021/02/12 15:10:16', '2021/02/12 15:28:31', '2021/02/12 14:16:37', 0, 'drsapnamarda@gmail.com', 0, 'aaefea72a2e1901cd201690c7042e3db'),
(358, 0, 0, 0, 1, 'CHETAN', 'MEHTA', '09974093659', 'RADIOLOGY DEPARTMENT, SSG HOSPITAL', 'VADODARA', NULL, NULL, '2021/02/12 14:19:38', 0, 'drchetan_mehta@yahoo.com', 0, '677591dc229e4a61176e58fc8b7b4269'),
(359, 0, 0, 0, 0, 'RASHMI', 'sudhir', '09849541987', 'Indo-american cancer hospital & research center', 'Manikonda, hyderabad', '2021/02/12 14:22:53', '2021/02/12 14:39:53', '2021/02/12 14:20:14', 0, 'rashmi4210@gmail.com', 0, '1e5422f2d0e53bb7b5db0cb39332abba'),
(360, 0, 0, 0, 1, 'Sunitha', 'C', '9489146561', 'Radiodiagnosis JIPMER', 'Reddiarpalayam', NULL, NULL, '2021/02/12 14:55:59', 0, 'sunithapradeepnair19@gmail.com', 0, '9984e2899065af6f183d741147ad27b4'),
(361, 0, 0, 0, 1, 'Anupreet ', 'Tandon ', '9811150476', 'Aakash healthcare ', 'New Delhi ', NULL, NULL, '2021/02/12 15:01:43', 0, 'anupreettandon@yahoo.com', 0, '50129bcd6f8be9617e349e2fdd49d75d'),
(362, 0, 0, 0, 0, 'Sinni', 'K v', '9495636726', 'Iria', 'Thrissur', NULL, NULL, '2021/02/12 15:02:42', 0, 'drsinnikv@gmail.com', 0, 'ade7153db0708db5f6cca754c7f95240'),
(363, 0, 0, 1, 0, 'Bhujang', 'Rao', '9177784988', 'Siemens', 'Hyderabad', NULL, NULL, '2021/02/12 15:08:13', 0, 'bhujanga.gundra@siemens-healthineers.com', 0, '4d80fe9d7a45b6cc53f9368a3d6c1f89'),
(364, 0, 0, 0, 1, 'Rajesh', 'S', '9446311566', 'Kims', 'Thiruvananthapuram', '2021/02/12 16:00:14', '2021/02/12 16:01:14', '2021/02/12 15:47:03', 0, 'rajeshmdmmc@hotmail.com', 0, '99007832401ff6cec20a28a4b0f60722'),
(365, 0, 0, 0, 1, 'Chaitanya', 'Puranik', '7746034768 ', 'Vishesh Jupiter Hospital Indore ', 'Indore', NULL, NULL, '2021/02/12 16:01:23', 0, 'chaitanyapuranik@yahoo.com', 0, 'ef498f561b1613fa4064e372104072ef'),
(366, 0, 0, 0, 1, 'Rajinder Kaur ', 'Saggu', '9871056323', 'Indraprastha Apollo Hospitals', 'New Delhi', '2021/02/12 16:41:50', '2021/02/12 17:24:04', '2021/02/12 16:39:51', 0, 'rksaggu13@gmail.com', 0, '6c796e9f716eb43c777c26198ed9d636'),
(367, 0, 0, 0, 0, 'Anusha', 'Varghese', '9847100397', 'Lourdes Hospital', 'Kochi', NULL, NULL, '2021/02/12 16:48:05', 0, 'dranushavarghese@gmail.com', 0, '89a5e0e5173889374a8ca67ec3fbf6f1'),
(368, 0, 0, 0, 0, 'Vaishali ', 'Upadhyaya', '9335252999', 'VPIMS', 'Lucknow', '2021/02/12 17:46:10', '2021/02/12 17:48:37', '2021/02/12 17:41:30', 0, 'vshali77@yahoo.co.in', 0, '62e4b3b15b750118e43bbd4128260d12'),
(369, 0, 0, 0, 1, 'Anuradha', 'Tuli', '9811174148', 'Images ultrasound and x ray clinic', 'Delhi', NULL, NULL, '2021/02/12 17:51:18', 0, 'anuradhatuli23@gmail.com', 0, '474a336d384aea0b68fe4bb986d0ab2c'),
(370, 0, 0, 0, 1, 'JIMIT', 'PATRAWALA', '09920039991', 'ARK IMAGING AND DIAGNOSTIC CENTRE', 'MUMBAI', NULL, NULL, '2021/02/12 18:07:48', 0, 'drjimitpatrawala@gmail.com', 0, 'ed759c86ef5f2d4926d72ab1db6d3799'),
(371, 0, 0, 0, 1, 'Divya', 'Santosh', '9901622247', 'Sri Shankara Cancer hospital and research center', 'Bengaluru', NULL, NULL, '2021/02/12 21:44:30', 0, 'divyasantosh@gmail.com', 0, 'e49a546fbc42194d8c599ce8c939cb18'),
(372, 0, 1, 0, 0, 'Kalai', 'Selvi S', '9677970547', 'Kmc Mangalore', 'Mangalore', '2021/02/14 21:25:54', '2021/02/14 21:26:18', '2021/02/14 21:22:39', 0, 'nks.kalaiselvi2000@gmail.com', 0, '66d472a8c58411132d8a3c57235db83f'),
(373, 0, 0, 0, 1, 'John V', 'Alexander', '09645675080', 'IGMC', 'Shimla', '2021/02/17 17:39:32', '2021/02/17 17:40:32', '2021/02/17 17:38:14', 1, 'vjohn989@gmail.com', 0, '588d2c36f92f0d68456bfa26db3aee89'),
(374, 0, 1, 0, 0, 'neeraj', 'reddy', '08367773537', 'coact', 'NELLORE', NULL, NULL, '2021/08/05 08:41:26', 0, 'pandurevanthreddy002@gmail.com', 0, 'badf3013625d2d61403511015bdccf76');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=375;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
