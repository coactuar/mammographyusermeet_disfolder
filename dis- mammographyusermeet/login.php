<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Mammography</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<style>
::-webkit-input-placeholder { /* Edge */
  color: black !important;
  font-weight: 400;
  font-family: Verdana, sans-serif;
 
  
}

:-ms-input-placeholder { /* Internet Explorer */
  color: black !important;
  font-weight: 400;
  font-family: Verdana, sans-serif;
  
}

::placeholder {
  color: black !important;
  font-weight: 400;
  font-family: Verdana, sans-serif;
  
}

video::-webkit-media-controls {
  display:none !important;
}
</style>
</head>

<body>
<video id="video"  style="display:none;" controls="false"  >
        <source src="videos/Seimens Healthineer Final HD Render with sound.mp4" type="video/3gpp" />
    </video>

<div class="container-fluid mr-1">

    <div class="row ">
      <div class="col-12 col-md-8  p-md-0 col-lg-8">
         <img src="img/Mammography.jpg" width="100%" height="100%vh"  alt=""/>
      </div>
      <div class="col-12 col-md-6 col-lg-4 text-center mt-md-2">
		<div class="container">
        <div class="login text-center">
        <img src="img/welcome.png" class="img-fluid text-left mt-lg-5" alt=""/>
				<form autocomplete="off"  id="login-form" method="post" role="form">
					  <div id="login-message"></div>
					  
					  <div class="form-group">   
						<input type="email"  class="form-control" placeholder="Username" name="email" id="email" required autocomplete='off' />	
					  </div>
					  <div class="form-group">   
					  <input  type="password" class="form-control" placeholder="Password" name="password" id="password" required autocomplete='off' />
					  </div>
					  <div class="form-group mt-2 text-left">
						<input type="image" src="img/Loginbutton.png" value="submit">
					  </div>
				</form>
            </div>
        
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">

function videoEnd() {
    var video = document.getElementById("video");
    video.webkitExitFullScreen();
    document.location = "webcast.php";
    video.style.display="none";
    
}

function playVideo() {
    var video = document.getElementById("video");
    video.style.display="block";
    video.addEventListener('ended', videoEnd, false);
    video.webkitEnterFullScreen();
    video.play();
}

</script>

<script>
$(document).on('submit', '#login-form', function()
{  $('#login').attr('disabled', true);
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        playVideo();
         
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
  });
  return false;
});
</script>

</body>
</html>