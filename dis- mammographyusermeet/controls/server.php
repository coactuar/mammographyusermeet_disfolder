<?php
require_once "config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        case 'getcountries':
            $sql = "SELECT * FROM countries";  
            $res = mysqli_query($link, $sql);
            $output='<select class="input" id="country" name="country" onChange="updateState()" required>';
            $output .= '<option value="0">Select Country</option>';
            while($data = mysqli_fetch_assoc($res)){
                $output .= '<option value="'.$data['id'].'">'.iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $data['name']).'</option>';
            }
            $output .= '</select>';
            
            echo $output;
        
        break;
        
        case 'getstates':
            $sql = "SELECT * FROM states where country_id='".$_POST['country']."' order by name";  
            $res = mysqli_query($link, $sql);
            $output='<select class="input" id="state" name="state" onChange="updateCity()" required>';
            $output .= '<option value="0">Select State</option>';
            while($data = mysqli_fetch_assoc($res)){
                $output .= '<option value="'.$data['id'].'">'.iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $data['name']).'</option>';
            }
            $output .= '</select>';
            
            echo $output;
        
        break;
        
        case 'getcities':
            $sql = "SELECT * FROM cities where state_id='".$_POST['state']."' order by name";  
            $res = mysqli_query($link, $sql);
            $output='<select class="input" id="city" name="city" required>';
            $output .= '<option value="0">Select City</option>';
            while($data = mysqli_fetch_assoc($res)){
                $output .= '<option value="'.$data['id'].'">'.iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $data['name']).'</option>';
            }
            $output .= '</select>';
            
            echo $output;
        
        break;
        
        

    }
    
}
    
    


?>